@if( session( 'error' ) )
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="alert alert-danger text-center">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                {{ session( 'error' ) }}
            </div>
        </div>
    </div>
@endif

@if( session( 'warning' ) )
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="alert alert-warning text-center">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                {{ session( 'warning' ) }}
            </div>
        </div>
    </div>
@endif

@if( session( 'success' ) )
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="alert alert-success text-center">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                {{ session( 'success' ) }}
            </div>
        </div>
    </div>
@endif

