<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>BuyByMe</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 20px;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .flex-left {
                align-items: start;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: left;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="position-ref full-height">
            <div class="content">
                <div class="title m-b-md">
                    BuyByMe
                </div>

            INFORMATIVA PRIVACY BUYBYME
                            <p class="flex-left">
                                Ai sensi del Regolamento UE 679/2016 ("GDPR"), BuyByMe S.R.L. ("Società" o il "Titolare") fornisce di seguito l'informativa riguardante il trattamento dei Suoi dati personali da Lei forniti nell'ambito dell’utilizzo della piattaforma di offerte online denominate BuyByMe. <br>                            1. Identità e dati di contatto del Titolare del trattamento<br>                            Il Titolare del trattamento è BuyByMe S.R.L., con sede legale in via Andrea Baldi 12, 00136 ROMA (RM) che può essere contattato al seguente indirizzo di posta elettronica dpo@buybyme.net<br>                            2. Dati di contatto del Responsabile della protezione dei dati (Data Protection Officer - DPO)<br>                            La Società ha designato un Responsabile della Protezione dei Dati, che può essere contattato al seguente indirizzo email dpo@buybyme.net.  3. Finalità del trattamento e base giuridica del trattamento <br>                            a. Finalità di legge e contrattuali necessarie – trattamento necessario per adempiere a un obbligo contrattuale o legale al quale è soggetto il titolare del trattamento ovvero per dare esecuzione ad una richiesta specifica dell'interessato<br>                            I Suoi dati personali potranno essere trattati, senza la necessità del Suo consenso, nei casi in cui ciò sia necessario per adempiere a obblighi derivanti da disposizioni di legge sia in materia civilistica che fiscale, di normativa comunitaria, nonché di norme, codici o procedure approvati da Autorità e altre Istituzioni competenti. Inoltre, i Suoi dati personali potranno essere trattati per dare seguito a richieste da parte dell'autorità amministrativa o giudiziaria competente e, più in generale, di soggetti pubblici nel rispetto delle formalità di legge. I Suoi dati personali inoltre saranno trattati per le finalità relative e/o connesse all'erogazione dei servizi da parte della Società, quali nello specifico:<br>                            - per l'esecuzione di obblighi derivanti dalle Condizioni Generali, e/o dalla erogazione dei servizi accessori e/o connessi a tali contratti. In tali casi, La informiamo che ai sensi della normativa applicabile in materia di dati personali, l'acquisizione del Suo consenso non è richiesta se il trattamento è necessario per eseguire obblighi derivanti da un contratto, mentre, in fase di negoziazione, non è altresì obbligatorio acquisire il consenso se il trattamento è necessario per adempiere – prima della conclusione del contratto – a Sue specifiche richieste;  - per l'erogazione dei servizi richiesti dal cliente con la registrazione sul sito internet e la creazione del proprio account e profilo inclusa la raccolta, la conservazione e la elaborazione dei dati ai fini dell'instaurazione e della successiva gestione operativa, tecnica ed amministrativa del rapporto (e dell'account e profilo creato dal cliente) connesso all'erogazione dei servizi e all’effettuazione di comunicazioni relative allo svolgimento dei servizi; <br>                            Tali dati – il cui conferimento è necessario ai fini dell’esecuzione operativa, economica ed amministrativa del servizio– saranno trattati anche con strumenti elettronici, registrati in apposite banche dati, e utilizzati strettamente ed esclusivamente nell’ambito del rapporto contrattuale in essere. Poiché la comunicazione dei Suoi dati per le predette finalità risulta necessaria al fine del mantenimento e dell'erogazione di tutti i servizi connessi al contratto, la mancata comunicazione renderà impossibile l'erogazione degli specifici servizi in questione.<br>                            b. Finalità commerciali e di marketing – consenso<br>                            I Suoi dati personali potranno altresì essere trattati, previo rilascio del Suo consenso, per le seguenti ulteriori finalità funzionali all'attività del Titolare o di un terzo:<br>                            - ricerche di mercato, analisi economiche e statistiche;  - commercializzazione dei servizi del Titolare e/o di un terzo, invio di materiale pubblicitario/informativo/promozionale e di partecipazione ad iniziative ed offerte volte a premiare i clienti del Titolare;  - rilevazioni del grado di soddisfazione della clientela sulla qualità dei servizi forniti.<br>                            Tali attività potranno riguardare prodotti e servizi del Titolare e potranno essere eseguite anche attraverso posta elettronica. Il consenso al trattamento dei dati e alla comunicazione ai soggetti di seguito indicati per le predette finalità è facoltativo e potrà essere revocato alla casella di mail dpo@buybyme.net<br>                            c. Difesa di un diritto in sede giudiziaria<br>                            In aggiunta, i suoi dati personali saranno trattati ogniqualvolta risulti necessario al fine di accertare, esercitare o difendere un diritto del Titolare in sede giudiziaria.<br>                            4.Destinatari dei dati personali<br>                            Per il perseguimento delle finalità indicate al punto 3, il Titolare potrà comunicare i Suoi dati personali a soggetti terzi, quali, ad esempio, quelli appartenenti ai seguenti soggetti o categorie di soggetti:<br>                            - forze di polizia, forze armate ed altre amministrazioni pubbliche, per l'adempimento di obblighi previsti dalla legge, da regolamenti o dalla normativa comunitaria. In tali ipotesi, in base alla normativa applicabile in tema di protezione dei dati è escluso l’obbligo di acquisire il previo consenso dell’interessato a dette comunicazioni; <br>                            Il Titolare garantisce la massima cura affinché la comunicazione dei Suoi dati personali ai predetti destinatari riguardi esclusivamente i dati necessari per il raggiungimento delle specifiche finalità cui sono destinati. I Suoi dati personali sono conservati nei database del Titolare e saranno trattati esclusivamente da parte di personale autorizzato. A questi ultimi saranno fornite apposite istruzioni sulle modalità e finalità del trattamento. Tali dati non saranno inoltre comunicati a terzi, salvo quanto sopra previsto e, in ogni caso, nei limiti ivi indicati. Infine, Le ricordiamo che i Suoi dati personali non saranno oggetto di diffusione, se non nei casi sopra descritti e/o previsti dalla legge.<br>                            6.Periodo di conservazione dei dati<br>                            I dati saranno conservati per un periodo di tempo non superiore a quello necessario agli scopi per i quali essi sono stati raccolti o successivamente trattati conformemente a quanto previsto dagli obblighi di legge.<br>                            I Suoi dati saranno conservati per dieci anni dal termine del rapporto in essere, al fine di permettere alla Società di difendersi da possibili pretese avanzate in relazione al contratto stesso. Al termine di tale periodo, saranno cancellati o altrimenti irreversibilmente de-identificati, salvo l'ulteriore conservazione di alcuni o tutti i dati sia richiesta dalla legge.<br>                            7.Diritti degli interessati <br>                            7.1 In qualità di interessato, Le sono riconosciuti i seguenti diritti sui dati personali raccolti e trattati dal Titolare per le finalità indicate al punto 3.<br>                            a. Diritto di accesso<br>                            Lei ha il diritto di ottenere dal Titolare del trattamento la conferma che sia o meno in corso un trattamento di dati personali che la riguarda e in tal caso, di ottenere l'accesso ai dati personali e alle seguenti informazioni: (i) le finalità del trattamento; (ii) le categorie di dati personali in questione; (iii) i destinatari o le categorie di destinatari a cui i dati personali sono stati o saranno comunicati, in particolare se destinatari di paesi terzi o organizzazioni internazionali; (iv) quando possibile, il periodo di conservazione dei dati personali previsto oppure, se non è possibile, i criteri utilizzati per determinare tale periodo; (v) il diritto di proporre reclamo a un'autorità di controllo.<br>                            b. Diritto di rettifica e cancellazione<br>                            Le è riconosciuto il diritto di ottenere la rettifica dei dati personali inesatti che la riguardano nonché, tenuto conto delle finalità del trattamento, il diritto di ottenere l'integrazione dei dati personali incompleti, anche fornendo una dichiarazione integrativa. Ha altresì il diritto di ottenere la cancellazione dei dati personali che la riguardano se sussiste uno dei seguenti motivi: (i) i dati personali non sono più necessari rispetto alle finalità per le quali sono stati raccolti o altrimenti trattati; (ii) i dati sono trattati illecitamente; (iii) ha revocato il consenso in base al quale il Titolare aveva il diritto di trattare i Suoi dati e non vi è altro fondamento giuridico che consente al Titolare l'attività di trattamento; (iv) si è opposto all'attività di trattamento e non c'è un motivo legittimo prevalente; (v) i dati personali devono essere cancellati per adempiere un obbligo legale. La Società ha tuttavia il diritto di disattendere l'esercizio dei suddetti diritti di cancellazione se prevale il diritto alla libertà di espressione e di informazione ovvero per l'esercizio di un obbligo di legge o per difendere un proprio diritto in giudizio.<br>                            Le sono riconosciuti altresì i seguenti diritti:<br>                            a) Diritto alla portabilità dei dati<br>                            Lei ha diritto di ricevere in un formato strutturato, di uso comune e leggibile i Suoi dati personali forniti alla Società e da questa trattati in base al consenso o altra base giuridica, nonché il diritto di trasmettere tali dati a un altro Titolare del trattamento senza impedimenti.<br>                            b) Diritto alla limitazione del trattamento<br>                            Lei ha il diritto di ottenere dalla Società la limitazione del trattamento quando ricorre una delle seguenti ipotesi: (i) per il periodo necessario al Titolare per verificare l'esattezza di tali dati personali che la riguardano di cui ha contestato l'esattezza; (ii) in caso di trattamento illecito dei Suoi dati personali; (iii) anche se i Suoi dati personali non sono necessari per le finalità del trattamento, in ogni caso ha la necessità che vengano trattati per l'accertamento, l'esercizio o la difesa di un diritto in sede giudiziaria; (iv) per il periodo necessario alla verifica in merito all'eventuale prevalenza dei motivi legittimi del Titolare rispetto alla Sua richiesta di opposizione al trattamento.<br>                            Può esercitare i diritti sopra elencati scrivendo al responsabile per la protezione dei dati dpo@buybyme.net. Le viene inoltre garantito il diritto di rivolgersi all'autorità per la protezione dei dati competente in caso di trattamento illecito dei Suoi dati.
                            </p>
            </div>
        </div>
    </body>
</html>
