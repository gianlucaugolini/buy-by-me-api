<body>
    <table>
        <thead>
            <tr>
                <th colspan="2">
                    <h1>
                        Nuova richiesta Acquisto Offerte
                    </h1>
                </th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>
                    <strong>
                        Utente
                    </strong>
                </td>
                <td>
                    {{ $data[ 'commerciant' ]->c_name . ' ' . $data[ 'commerciant' ]->c_last_name }}

                    <a href="https://admin1.buybyme.net/public/users/details/{{ $data[ 'commerciant' ]->user_id}}">
                        clicca per visualizzare i dettagli del commerciante
                    </a>
                </td>
                <td>

                </td>
            </tr>
            <tr>
                <td>
                    <strong>
                        Numero offerte richieste
                    </strong>
                </td>
                <td>
                    {{ $data[ 'offers' ] }}
                </td>
            </tr>
        </tbody>
    </table>
</body>
