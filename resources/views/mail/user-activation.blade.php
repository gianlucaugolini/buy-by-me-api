<body>
<table>
    <thead>
    <tr>
        <th colspan="2">
            <h4>
                Gentile utente, ti ringraziamo per esserti registrato a BuyByMe: l’app che ti permette di scoprire offerte vicino a te in tempo reale.
            </h4>
        </th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>
            Potrai:<br>
            <ul>
                <li>Scoprire gli esercizi commerciali attivi sulla mappa</li>
                <li>Vedere le offerte disponibili e la loro scadenza cliccando su un negozio</li>
                <li>Salvarti le tue offerte preferite per poi ritrovarle più tardi</li>
                <li>Trovare il percorso più rapido per recarti al negozio</li>
                <li>Configurare le tue preferenze di categorie e tipologie di sconti</li>
                <li>Inviarci feedback per migliorare l’app</li>
            </ul>
            <br>
            Affrettati perche’ spesso le offerte hanno scadenze molto brevi e solo con BuyBYMe le potrai scoprire ed afferrare al volo.
        </td>
    </tr>

    <tr>
        <td>
            <br>
            Per confermare la tua mail clicca qui: <a href="{{ route( 'activate-account', [ 'activation_token' => $user->activation_token ] ) }}">LINK</a>
            <br><br>
            Grazie e benvenuto dal team di BuyByMe.
        </td>
    </tr>
    </tbody>
</table>
</body>
