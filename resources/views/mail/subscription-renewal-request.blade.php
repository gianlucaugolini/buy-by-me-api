<body>
    <table>
        <thead>
            <tr>
                <th colspan="2">
                    <h3>
                        Nuova richiesta di rinnovo Abbonamento da {{ $data[ 'commerciant' ]->c_name . ' ' . $data[ 'commerciant' ]->c_last_name }}
                    </h3>
                </th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>
                    {{ $data[ 'commerciant' ]->c_name . ' ' . $data[ 'commerciant' ]->c_last_name }} ha richiesto informazioni relative allo stato/aggiornamento/rinnovo del suo abbonamento.

                    <a href="https://admin1.buybyme.net/public/users/details/{{ $data[ 'commerciant' ]->user_id}}">
                        clicca per visualizzare i dettagli del commerciante
                    </a>
                </td>
                <td>

                </td>
            </tr>
        </tbody>
    </table>
</body>
