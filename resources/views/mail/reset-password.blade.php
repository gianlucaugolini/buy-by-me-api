<body>
    <table>
        <thead>
            <tr>
                <th colspan="2">
                    <h1>
                        Segui il link per ripristinare la tua password
                    </h1>
                </th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>
                    Abbiamo ricevuto una richiesta di reset della password, <a href="{{ route( 'reset-password', [ 'reset_token' => $user->api_password_reset_token ] ) }}">clicca qui</a> per effettuare la procedura.
                </td>
            </tr>
        </tbody>
    </table>
</body>
