<body>
    <table>
        <thead>
            <tr>
                <th colspan="2">
                    <h1>
                        Nuovo feedback utente
                    </h1>
                </th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>
                    <strong>
                        Utente
                    </strong>
                </td>
                <td>{{ $data[ 'user' ]->fullname }}
                    <a href="https://admin1.buybyme.net/public/users/details/{{ $data[ 'user' ]->id}}">
                        clicca per visualizzare i dettagli del cliente
                    </a>
                </td>
            </tr>
            <tr>
                <td>
                    <strong>
                        Email
                    </strong>
                </td>
                <td>
                    {{ $data[ 'user' ]->email }}

                </td>
            </tr>
            <tr>
                <td>
                    <strong>
                        Messaggio
                    </strong>
                </td>
                <td>
                    {{ $data[ 'feedback' ] }}
                </td>
            </tr>
        </tbody>
    </table>
</body>
