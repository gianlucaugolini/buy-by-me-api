<body>
<table>
    <thead>
    <tr>
        <th colspan="2">
            <h4>
                Gentile commerciante, ti ringraziamo per esserti registrato a BuyByMe: l’app che ti permette di pubblicare le tue offerte vicino in tempo reale e scoprire nuovi potenziali clienti in pochi attimi.
            </h4>
        </th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>
            Potrai:<br>
            <ul>
                <li>richiedere l’acquisto di pacchetti di offerta direttamente dall’app per essere pronto a pubblicare</li>
                <li>esporre un’offerta con informazioni su categoria merceologica, quantità, sconto e scadenza in pochi secondi</li>
                <li>sapere sempre quanti utenti hanno visualizzato ogni offerta</li>
                <li>ricevere nuovi clienti in tempo reale</li>
            </ul>
            <br>
            BuyByMe e’ un’occasione unica per acquisire nuova visibilità per il tuo esercizio commerciale.
        </td>
    </tr>

    <tr>
        <td>
            <br>
            Per confermare la tua mail clicca qui: <a href="{{ route( 'activate-account', [ 'activation_token' => $user->activation_token ] ) }}">LINK</a> e verrai contattato immediatamente dal team BuyByMe.
            <br><br>
            Grazie e benvenuto.
        </td>
    </tr>
    </tbody>
</table>
</body>
