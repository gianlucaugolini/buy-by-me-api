<body>
    <table>
        <thead>
            <tr>
                <th colspan="2">
                    <h1>
                        Nuova Offerta pubblicata
                    </h1>
                </th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>
                    <strong>
                        Utente
                    </strong>
                </td>
                <td>
                    {{ $commerciant->c_name . ' ' . $commerciant->c_last_name }}
                </td>
            </tr>
            <tr>
                <td>
                    <strong>
                        Negozio
                    </strong>
                </td>
                <td>
                    {{ $commerciant->shop->s_name }}
                </td>
            </tr>
            <tr>
                <td>
                    <strong>
                        Nuova offerta
                    </strong>
                </td>
                <td>
                    {{ $offer->o_title }}
                </td>
            </tr>
            <tr>
                <td>
                    <strong>
                        Descrizione
                    </strong>
                </td>
                <td>
                    {{ $offer->o_desc}}
                </td>
            </tr>
            <tr>
                <td>
                    <strong>
                        Prezzo
                    </strong>
                </td>
                <td>
                    {{ $offer->o_pric}}€
                </td>
            </tr>
            <tr>
                <td>
                    <strong>
                        Sconto dichiarato
                    </strong>
                </td>
                <td>
                    {{ $offer->o_disc}}
                </td>
            </tr>
            <tr>
                <td>
                    <strong>
                        Numero colli
                    </strong>
                </td>
                <td>
                    {{ $offer->o_howm}}
                </td>
            </tr>
            <tr>
                <td>
                    <strong>
                        Inizio offerta
                    </strong>
                </td>
                <td>
                    {{ date( 'd/m/Y H:i:s', $offer->o_start ) }}
                </td>
            </tr>
            <tr>
                <td>
                    <strong>
                        Fine offerta
                    </strong>
                </td>
                <td>
                    {{ date( 'd/m/Y H:i:s', $offer->o_end ) }}
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <a href="https://admin1.buybyme.net/public/offers/details/{{ $offer->id }}">clicca per visualizzare</a>
                </td>
            </tr>
        </tbody>
    </table>
</body>
