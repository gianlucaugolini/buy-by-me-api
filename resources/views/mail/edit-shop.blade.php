<body>
    <table width="600" align="center" class="main" style="font-size: 16px; background-color: #FF9E51; width: 660px; border: 1px solid black; border-collapse: collapse;">
        <thead>
            <tr>
                <th colspan="2" style="border: 1px solid black; border-collapse: collapse; color: white; background-color: black;">
                    <h2>
                        Modifica informazioni Negozio
                    </h2>
                </th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td style="border: 1px solid black; border-collapse: collapse;">
                    <table width="100%" class="sub-table">
                        <tr align="left">
                            <th width="20%">
                                Attributo
                            </th>
                            <th width="40%" style="padding: 10px;">Vecchio valore</th>
                            <th width="40%" style="padding: 10px;">Nuovo valore</th>
                        </tr>
                        <tr style="background-color: #fff;">
                            <td style="padding: 10px;">
                                <strong>Nome</strong>
                            </td>
                            <td style="padding: 10px;">
                                <?php echo $old_data['old_user']->fullname ?>
                            </td>
                            <td style="padding: 10px;">
                                <?php echo $user->fullname ?>
                            </td>
                        </tr>
                        <tr style="background-color: #e2e3e5;">
                            <td style="padding: 10px;">
                                <strong>Email</strong>
                            </td>
                            <td style="padding: 10px;">
                                <?php echo $old_data['old_user']->email ?>
                            </td>
                            <td style="padding: 10px;">
                                <?php echo $user->email ?>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="border: 1px solid black; border-collapse: collapse;">
                    <table width="100%" class="sub-table">
                        <tr align="left">
                            <th width="20%">
                                Attributo
                            </th>
                            <th width="40%" style="padding: 10px;">Vecchio valore</th>
                            <th width="40%" style="padding: 10px;">Nuovo valore</th>
                        </tr>
                        <tr style="background-color: #fff;">
                            <td style="padding: 10px;">
                                <strong>Nome</strong>
                            </td>
                            <td style="padding: 10px;">
                                <?php echo $old_data['old_commerciant']->c_name ?>
                            </td>
                            <td style="padding: 10px;">
                                <?php echo $commerciant->c_name ?>
                            </td>
                        </tr>
                        <tr style="background-color: #e2e3e5;">
                            <td style="padding: 10px;">
                                <strong>Cognome</strong>
                            </td>
                            <td style="padding: 10px;">
                                <?php echo $old_data['old_commerciant']->c_last_name ?>
                            </td>
                            <td style="padding: 10px;">
                                <?php echo $commerciant->c_last_name ?>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="border: 1px solid black; border-collapse: collapse;">
                    <table width="100%" class="sub-table">
                        <tr align="left">
                            <th width="20%">
                                Attributo
                            </th>
                            <th width="40%" style="padding: 10px;">Vecchio valore</th>
                            <th width="40%" style="padding: 10px;">Nuovo valore</th>
                        </tr>
                        <tr style="background-color: #fff;">
                            <td style="padding: 10px;">
                                <strong>Nome Negozio</strong>
                            </td>
                            <td style="padding: 10px;">
                                <?php echo $old_data['old_shop']->s_name ?>
                            </td>
                            <td style="padding: 10px;">
                                <?php echo $shop->s_name ?>
                            </td>
                        </tr>
                        <tr style="background-color: #e2e3e5;">
                            <td style="padding: 10px;">
                                <strong>Descrizione</strong>
                            </td>
                            <td style="padding: 10px;">
                                <?php echo $old_data['old_shop']->s_desc ?>
                            </td>
                            <td style="padding: 10px;">
                                <?php echo $shop->s_desc ?>
                            </td>
                        </tr>
                        <tr style="background-color: #fff;">
                            <td style="padding: 10px;">
                                <strong>Prodotti</strong>
                            </td>
                            <td style="padding: 10px;">
                                <?php echo $old_data['old_shop']->s_prd ?>
                            </td>
                            <td style="padding: 10px;">
                                <?php echo $shop->s_prd ?>
                            </td>
                        </tr>
                        <tr style="background-color: #e2e3e5;">
                            <td style="padding: 10px;">
                                <strong>Indirizzo</strong>
                            </td>
                            <td style="padding: 10px;">
                                <?php echo $old_data['old_shop']->s_address ?>
                            </td>
                            <td style="padding: 10px;">
                                <?php echo $shop->s_address ?>
                            </td>
                        </tr>
                        <tr style="background-color: #fff;">
                            <td style="padding: 10px;">
                                <strong>CAP</strong>
                            </td>
                            <td style="padding: 10px;">
                                <?php echo $old_data['old_shop']->s_cap ?>
                            </td>
                            <td style="padding: 10px;">
                                <?php echo $shop->s_cap ?>
                            </td>
                        </tr>
                        <tr style="background-color: #e2e3e5;">
                            <td style="padding: 10px;">
                                <strong>Citt&agrave;</strong>
                            </td>
                            <td style="padding: 10px;">
                                <?php echo $old_data['old_shop']->s_city ?>
                            </td>
                            <td style="padding: 10px;">
                                <?php echo $shop->s_city ?>
                            </td>
                        </tr>
                        <tr style="background-color: #fff;">
                            <td style="padding: 10px;">
                                <strong>Provincia</strong>
                            </td>
                            <td style="padding: 10px;">
                                <?php echo $old_data['old_shop']->s_prov ?>
                            </td>
                            <td style="padding: 10px;">
                                <?php echo $shop->s_prov ?>
                            </td>
                        </tr>
                        <tr style="background-color: #e2e3e5;">
                            <td style="padding: 10px;">
                                <strong>Cod. Prov.</strong>
                            </td>
                            <td style="padding: 10px;">
                                <?php echo $old_data['old_shop']->s_prov_code ?>
                            </td>
                            <td style="padding: 10px;">
                                <?php echo $shop->s_prov_code ?>
                            </td>
                        </tr>
                        <tr style="background-color: #fff;">
                            <td style="padding: 10px;">
                                <strong>Lat.</strong>
                            </td>
                            <td style="padding: 10px;">
                                <?php echo $old_data['old_shop']->s_lat ?>
                            </td>
                            <td style="padding: 10px;">
                                <?php echo $shop->s_lat ?>
                            </td>
                        </tr>
                        <tr style="background-color: #e2e3e5;">
                            <td style="padding: 10px;">
                                <strong>Long.</strong>
                            </td>
                            <td style="padding: 10px;">
                                <?php echo $old_data['old_shop']->s_lon ?>
                            </td>
                            <td style="padding: 10px;">
                                <?php echo $shop->s_lon ?>
                            </td>
                        </tr>
                        <tr style="background-color: #fff;">
                            <td style="padding: 10px;">
                                <strong>Categoria</strong>
                            </td>
                            <td style="padding: 10px;">
                                <?php echo $old_cat->name ?>
                            </td>
                            <td style="padding: 10px;">
                                <?php echo $new_cat->name ?>
                            </td>
                        </tr>
                        <tr style="background-color: #e2e3e5;">
                            <td style="padding: 10px;">
                                <strong>Catena Commerciale</strong>
                            </td>
                            <td style="padding: 10px;">
                                <?php echo !is_null($old_catena) ? $old_catena->cc_name : '---------' ?>
                            </td>
                            <td style="padding: 10px;">
                                <?php echo !is_null($new_catena) ? $new_catena->cc_name : '---------' ?>
                            </td>
                        </tr>
                        <tr style="background-color: #fff;">
                            <td style="padding: 10px;">
                                <strong>Centro commerciale</strong>
                            </td>
                            <td style="padding: 10px;">
                                <?php echo !is_null($old_cc) ? $old_cc->cc_name : '---------' ?>
                            </td>
                            <td style="padding: 10px;">
                                <?php echo !is_null($new_cc) ? $new_cc->cc_name : '---------' ?>
                            </td>
                        </tr>
                        <tr style="background-color: #e2e3e5;">
                            <td style="padding: 10px;">
                                <strong>Piano CC</strong>
                            </td>
                            <td style="padding: 10px;">
                                <?php echo !is_null($old_data['old_shop']->s_centro_commerciale_piano) ? $old_data['old_shop']->s_centro_commerciale_piano : '------------' ?>
                            </td>
                            <td style="padding: 10px;">
                                <?php echo !is_null($shop->s_centro_commerciale_piano) ? $shop->s_centro_commerciale_piano : '------------' ?>
                            </td>
                        </tr>
                        <tr style="background-color: #fff;">
                            <td style="padding: 10px;">
                                <strong>Sito web</strong>
                            </td>
                            <td style="padding: 10px;">
                                <?php echo !is_null($old_data['old_shop']->s_web) ? $old_data['old_shop']->s_web : '------------' ?>
                            </td>
                            <td style="padding: 10px;">
                                <?php echo !is_null($shop->s_web) ? $shop->s_web : '------------' ?>
                            </td>
                        </tr>
                        <tr style="background-color: #e2e3e5;">
                            <td style="padding: 10px;">
                                <strong>Telefono</strong>
                            </td>
                            <td style="padding: 10px;">
                                <?php echo !is_null($old_data['old_shop']->s_phone) ? $old_data['old_shop']->s_phone !== '' ? $old_data['old_shop']->s_phone : '------------' : '------------' ?>
                            </td>
                            <td style="padding: 10px;">
                                <?php echo !is_null($shop->s_phone) ? $shop->s_phone !== '' ? $shop->s_phone : '------------' : '------------' ?>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr align="center">
                <td colspan="2" style="padding: 10px;">
                    <a style="font-size: 14px; color: #000;" href="https://admin1.buybyme.net/public/shops/details/{{ $shop->id }}">clicca per visualizzare i dettagli del negozio</a>
                </td>
            </tr>
        </tbody>
    </table>
</body>
