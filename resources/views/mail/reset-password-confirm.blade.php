<body>
    <table>
        <thead>
            <tr>
                <th colspan="2">
                    <h1>
                        Reset della password confermata.
                    </h1>
                </th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>
                    La procedura di reset della password è avvenuta con successo, ora puoi accedere con le nuove credenziali che hai inserito.
                </td>
            </tr>
        </tbody>
    </table>
</body>
