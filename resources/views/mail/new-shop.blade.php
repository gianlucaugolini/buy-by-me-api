<body>
    <table>
        <thead>
            <tr>
                <th colspan="2">
                    <h1>
                        Nuova iscrizione Negozio
                    </h1>
                </th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>
                    <strong>
                        Negozio
                    </strong>
                </td>
                <td>
                    {{ $shop->s_name }}
                </td>
            </tr>
            <tr>
                <td>
                    <strong>
                        Commerciante
                    </strong>
                </td>
                <td>
                    {{ $commerciant->c_name . ' ' . $commerciant->c_last_name }}
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <a href="https://admin1.buybyme.net/public/shops/details/{{ $shop->id }}">clicca per visualizzare i dettagli del negozio</a>
                </td>
            </tr>
        </tbody>
    </table>
</body>
