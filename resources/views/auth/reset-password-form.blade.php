<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>BuyByMe</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <style>
        html, body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Raleway', sans-serif;
            font-weight: 100;
            height: 100vh;
            margin: 20px;
        }

        .full-height {
            height: 100vh;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .flex-left {
            align-items: start;
            display: flex;
            justify-content: center;
        }

        .flex-right {
            align-items: end;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
            width: 400px;
        }

        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;
        }

        .content {
            text-align: left;
        }

        .title {
            font-size: 84px;
        }

        .links > a {
            color: #636b6f;
            padding: 0 25px;
            font-size: 12px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }

        .m-b-md {
            margin-bottom: 30px;
        }
    </style>
</head>
<body>
<div class="full-height">
    <div class="container">
        @include( 'ui.form-validation-messages' )
        @include( 'ui.messages' )

        <div class="row mb-2">
            <div class="col-sm">
                <div class="card flex-center">
                    <div class="card-body"><br>
                        <img alt="BuyByMe" src="/public/storage/logo.png"  width="400px" />
                        <br><br>
                        <form name="reset_password_form" method="post" action="{{ route( 'save-new-password' ) }}">
                            @csrf
                            <input type="hidden" name="activation_token" value="{{ Request::route('reset_token') }}">
                            <div class="row mb-2">
                                <div class="col-sm">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="form-row">
                                                <div class="form-group col">
                                                    <label for="first_name">
                                                        Nuova Password
                                                    </label>
                                                    <input type="password" name="new_password" class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="form-row">
                                            <div class="form-group col">
                                                <label for="first_name">
                                                    Ripeti Password
                                                </label>
                                                <input type="password" name="new_password_confirmation" class="form-control">
                                            </div>
                                        </div>
                                        <br>
                                        <div class="form-row">
                                            <div class="form-group col flex-right ">
                                                <label for="confirm_button">
                                                    Conferma
                                                </label>
                                                <input type="submit" name="confirm_action_button" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
