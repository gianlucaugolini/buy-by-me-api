<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get( 'activate-account/{activation_token}', 'UsersController@activateAccount' )->name( 'activate-account' );
Route::get( 'reset-password/{reset_token}', 'UsersController@resetPassword' )->name( 'reset-password' );
Route::post( 'save-new-password', 'UsersController@saveNewPassword' )->name( 'save-new-password' );

