<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([

    'middleware' => [
        'api',
        'jwt.auth',
        'recordMobileToken'
    ],
    'prefix' => 'auth'

], function ( $router ) {
    Route::get( 'logout', 'AuthController@logout' );
    Route::post( 'refresh', 'AuthController@refresh' );
    Route::post( 'me', 'AuthController@me' );
    // Route::get( 'user', 'AuthController@getAuthenticatedUser' );

});


Route::group( [

    'middleware' => [
        'api',
        'recordMobileToken'
    ],
    'prefix' => 'auth'
], function () {

    //Route::post( 'logout', 'AuthController@logout' );
    Route::post( 'login', 'AuthController@login' );
    Route::post( 'register', 'AuthController@register' );
    Route::post( 'request-reset-password', 'UsersController@requestResetPassword' )->name( 'request-reset-password' );
    Route::post( 'save-new-password', 'UsersController@saveNewPassword' )->name( 'save-new-password' );


} );

Route::group( [

    'middleware'    => 'api',
    'prefix'        => 'push'

], function () {
    Route::get( 'send/{offer_id}', 'PushController@sendDebug' );
} );


Route::group( [

    'middleware'    => 'api',
    'prefix'        => 'shops'

], function () {
    Route::get( '/details/{shop_id}', 'ShopsController@details' );
    Route::get( '/shops-list', 'ShopsController@all' );
    Route::post( '/new', 'ShopsController@createFull' );
    Route::post( '/new_api', 'ShopsController_API_KEY@createFull' );
} );


Route::group( [

    'middleware' => [
        'api',
        'jwt.auth',
        'recordMobileToken'
    ],
    'prefix'        => 'shops'

], function () {
    Route::post( '/edit', 'ShopsController@edit' );
} );

Route::group( [

    'middleware'    => 'api',
    'prefix'        => 'offers'

], function () {
    Route::get( '/all', 'OffersController@all' );
    Route::get( '/details/{offer_id}', 'OffersController@details' );
    Route::post( '/set-views', 'OffersController@setviews' );
    Route::post( '/new', 'OffersController@new' );
    Route::post( '/cancel', 'OffersController@cancel' );
    Route::post( '/edit', 'OffersController@edit' );
    Route::post( '/buy-more', 'OffersController@buyMore' ); //todo remove in future
    Route::post( '/renewal', 'OffersController@renewal' );

} );

Route::group( [

    'middleware'    => 'api',
    'prefix'        => 'customers'

], function () {
    Route::post( '/new', 'CustomersController@createFull' );
    Route::post( '/favs', 'CustomersController@allFavs' );
    Route::post( '/favs/set', 'CustomersController@setFav' );
    Route::post( '/prefs/set', 'CustomersController@setPrefs' );

    Route::post( '/likes', 'CustomersController@allLikes' );
    Route::post( '/likes/set', 'CustomersController@setLike' );

} );

Route::group( [

    'middleware'    => 'api',
    'prefix'        => 'centers'

], function () {
    Route::get( '/all', 'CentriCommercialiController@all' );
} );

Route::group( [

    'middleware'    => 'api',
    'prefix'        => 'malls'

], function () {
    Route::get( '/all', 'CatenaController@all' );
} );

Route::group( [

    'middleware' => [
        'api',
        'checklocal'
    ],
    'prefix'        => 'cache'

], function () {
    Route::get( '/flush', 'CacheController@flush');
} );

Route::group( [

    'middleware'    => 'api',
    'prefix'        => 'feedbacks'

], function () {
    Route::post( '/send', 'FeedbacksController@send' );
} );

/*Route::filter('localCallOnly', function()
{
    //if IPs don't match - 404
    if (Request::server('SERVER_ADDR') != Request::server('REMOTE_ADDR'))
    {
        return App::abort(404);
    }
});*/
