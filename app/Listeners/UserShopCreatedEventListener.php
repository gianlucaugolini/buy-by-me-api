<?php

namespace App\Listeners;

use App\Events\UserShopCreatedEvent;
use App\Mail\UserShopActivationLink;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

class UserShopCreatedEventListener {
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct() {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle( UserShopCreatedEvent $event ) {
        
        Mail::to( $event->user->email )->queue( new UserShopActivationLink( $event->user ) );

    }
}
