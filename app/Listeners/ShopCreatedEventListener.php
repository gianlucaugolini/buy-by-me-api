<?php

namespace App\Listeners;

use App\Events\ShopCreatedEvent;
use App\Mail\NewShop;
use App\Models\User;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;
use Log;

class ShopCreatedEventListener {
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct() {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle( ShopCreatedEvent $event ) {

        //$admin_emails = User::byTypeId( User::ADMINISTRATOR )->get()->pluck( 'email' )->toArray();

        $admin_emails = $event->debug ? ["neogene@gmail.com"] : ["neogene@gmail.com","info@buybyme.net"];// explode(';', env("NOTIFICATION_EMAIL",'info@buybyme.net' ) );

        //Log::debug("NEW SHOP EMAIL SENDING to: ".json_encode($admin_emails)." DEBUG: ".$event->debug);

        Mail::to( $admin_emails )->queue( new NewShop( $event->shop, $event->commerciant ) );

    }
}
