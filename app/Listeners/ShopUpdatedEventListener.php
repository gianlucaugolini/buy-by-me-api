<?php

namespace App\Listeners;


use App\Events\ShopUpdatedEvent;
use App\Mail\UpdateShopAndCommerciantDetails;
use Illuminate\Support\Facades\Mail;
use Log;

class ShopUpdatedEventListener {
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct() {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle( ShopUpdatedEvent $event ) {

        //$admin_emails = User::byTypeId( User::ADMINISTRATOR )->get()->pluck( 'email' )->toArray();

        // $admin_emails = ['gianluca.ugolini@gmail.com', 'pogo2k@libero.it'];
        $admin_emails = $event->debug ? ["neogene@gmail.com", 'gianluca.ugolini@gmail.com', 'pogo2k@libero.it'] : ["neogene@gmail.com","info@buybyme.net"];// explode(';', env("NOTIFICATION_EMAIL",'info@buybyme.net' ) );

        Log::debug("UPDATED SHOP EMAIL SENDING to: ".json_encode($admin_emails)." DEBUG: ".$event->debug);

        Mail::to( $admin_emails )->queue( new UpdateShopAndCommerciantDetails( $event->old_data, $event->shop, $event->commerciant, $event->user ) );

    }
}
