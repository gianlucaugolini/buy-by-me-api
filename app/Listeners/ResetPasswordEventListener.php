<?php

namespace App\Listeners;

use App\Events\ResetPasswordEvent;
use App\Mail\ResetPasswordLink;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;
use Log;

class ResetPasswordEventListener {
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct() {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle( ResetPasswordEvent $event ) {
        Mail::to( $event->user->email )->queue( new ResetPasswordLink( $event->user ) );

        Log::debug("RESET EMAIL SENT TO: ".$event->user->email);
    }
}
