<?php

namespace App\Listeners;

use App\Events\OfferCreatedEvent;
use App\Mail\OfferPublished;
use App\Models\User;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;
use Log;

class OfferCreatedEventListener {
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct() {
        //
       // Log::debug("NEW OFFER EVENT LISTENER CREATED... ");

    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle( OfferCreatedEvent $event ) {

        //$admin_emails = User::byTypeId( User::ADMINISTRATOR )->get()->pluck( 'email' )->toArray();

        //Log::debug("NEW OFFER EMAIL SENDING TO... ");

        $admin_emails = $event->debug ? ["neogene@gmail.com"] : ["neogene@gmail.com","info@buybyme.net"];// explode(';', env("NOTIFICATION_EMAIL",'info@buybyme.net' ) );

        //Log::debug("NEW OFFER EMAIL SENDING TO: ".json_encode($admin_emails));

        Mail::to( $admin_emails )->queue( new OfferPublished( $event->offer, $event->commerciant ) );

        //Log::debug("NEW OFFER EMAIL SENT!");


    }
}
