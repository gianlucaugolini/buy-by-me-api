<?php

namespace App\Exceptions;

use App\Traits\ApiResponser;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\Response;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

class Handler extends ExceptionHandler {

    use ApiResponser;

    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception $exception
     * @return void
     * @throws Exception
     */
    public function report( Exception $exception ) {
        parent::report( $exception );
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Exception $e
     * @return \Illuminate\Http\Response
     */
    public function render( $request, Exception $e ) {

        $messages = [];


        if( $e instanceof NotFoundHttpException ){

            $messages = [
                'message'   => 'Route not found.'
            ];

            return $this->errorResponse( $messages, Response::HTTP_NOT_FOUND );

        } else if( $e instanceof ModelNotFoundException ) {

            $messages = [
                'message'   => 'Requested Resource could not be found in the database.',
                'class'     =>  str_replace('App\Models\\', '', $e->getModel()),
            ];

            return $this->errorResponse( $messages, Response::HTTP_NOT_FOUND );

        } else if( $e instanceof QueryException ) {

            $messages = [
                'message'   => 'Error connecting to database.' . $e->getMessage()
            ];

            return $this->errorResponse( $messages, Response::HTTP_INTERNAL_SERVER_ERROR );

        } else if( $e instanceof MethodNotAllowedHttpException ) {

            $messages = [
                'message'   => 'Invalid request method.'
            ];

            return $this->errorResponse( $messages, Response::HTTP_METHOD_NOT_ALLOWED );

        }  else if( $e instanceof UnauthorizedHttpException ) {

            $messages = [
                'message'   => 'Authentication not Valid.'
            ];

            return $this->errorResponse( $messages, Response::HTTP_UNAUTHORIZED );

        } else {

            return parent::render( $request, $e );

        }

        // if ($e instanceof HttpResponseException) {
        //     return $e->getResponse();
        // } elseif ($e instanceof ModelNotFoundException) {
        //     $e = new NotFoundHttpException($e->getMessage(), $e);
        // } elseif ($e instanceof AuthorizationException) {
        //     $e = new HttpException(403, $e->getMessage());
        // } elseif ($e instanceof ValidationException && $e->getResponse()) {
        //     return $e->getResponse();
        // }

    }
}
