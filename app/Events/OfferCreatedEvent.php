<?php

namespace App\Events;

use App\Models\Commerciant;
use App\Models\Offer;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use App\Helpers\PushNotificationsHelper;

class OfferCreatedEvent {
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $offer;
    public $commerciant;
    public $debug = false;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct( Offer $offer, Commerciant $commerciant, $debug = false ) {
        $this->offer        = $offer;
        $this->commerciant  = $commerciant;
        $this->debug        = $debug;

        PushNotificationsHelper::sendPushNotificationForOfferWithID($offer->id, $debug);
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn() {
        return new PrivateChannel('channel-name');
    }
}
