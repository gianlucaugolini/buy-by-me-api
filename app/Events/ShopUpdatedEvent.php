<?php

namespace App\Events;

use App\Models\Commerciant;
use App\Models\Shop;
use App\Models\User;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class ShopUpdatedEvent {
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $shop;
    public $commerciant;
    public $user;
    public $old_data;
    public $debug = false;

    /**
     * Create a new event instance.
     *
     * @param Shop $shop
     * @param Commerciant $commerciant
     * @param bool $debug
     */
    public function __construct( $old_data, Shop $shop, Commerciant $commerciant, User $user, $debug = false ) {
        $this->shop         = $shop;
        $this->commerciant  = $commerciant;
        $this->user         = $user;
        $this->old_data     = $old_data;
        $this->debug        = $debug;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn() {
        return new PrivateChannel('channel-name');
    }
}
