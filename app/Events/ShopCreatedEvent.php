<?php

namespace App\Events;

use App\Models\Commerciant;
use App\Models\Shop;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class ShopCreatedEvent {
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $shop;
    public $commerciant;
    public $debug = false;

    /**
     * Create a new event instance.
     *
     * @param Shop $shop
     * @param Commerciant $commerciant
     * @param bool $debug
     */
    public function __construct( Shop $shop, Commerciant $commerciant,  $debug = false ) {
        $this->shop        = $shop;
        $this->commerciant  = $commerciant;
        $this->debug        = $debug;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn() {
        return new PrivateChannel('channel-name');
    }
}
