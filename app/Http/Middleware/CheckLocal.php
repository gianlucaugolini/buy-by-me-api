<?php

namespace App\Http\Middleware;

use App\Models\User;
use Closure;
use Illuminate\Support\Facades\Auth;

class CheckLocal{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle( $request, Closure $next ) {

        if (Request::server('SERVER_ADDR') != Request::server('REMOTE_ADDR'))
        {
            return App::abort(404);
        }

        return $next($request);
    }
}
