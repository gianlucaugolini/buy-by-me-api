<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Models\Customer;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Traits\ApiResponser;

class RecordMobileToken{

    use ApiResponser;

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle( $request, Closure $next ) {

            $response = $next($request);

            $token  = null;
            $ios = false;

            if ($request->has('u_notification_ios_token')) {
                $token = $request->u_notification_ios_token;
                $ios = true;
            }
            else if ($request->has('u_notification_android_token')) {
                $token = $request->u_notification_android_token;
                $ios = false;
            }

           // Log::debug(var_dump($request->all()));

            if (isset($token) && $token!==null){

                //Log::debug("TOKEN:$token");

                try { //aggiorna il token

                    $user = auth()->user();

                    //Log::debug("USER:".var_dump($user));

                    // inizializza un transazione
                    DB::beginTransaction();

                    $customer_details = Customer::byUserId( $user->id )->firstOrFail();

                    //Log::debug("CUSTOMER:".var_dump($customer_details));

                    if ($ios) {
                        $customer_details->u_notification_ios_token = $token;
                    }
                    else {
                        $customer_details->u_notification_android_token = $token;
                    }

                    $customer_details->save();

                    // committa la transazione
                    DB::commit();

                } catch (\Exception $e) { // se genera eccezione

                }
            }

        return $response;
    }
}
