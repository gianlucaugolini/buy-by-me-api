<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
namespace App\Http\Controllers;

use App\Models\CentroCommerciale;

class CentriCommercialiController extends Controller {

    /**
     * Create a new CentriCommercialiController instance.
     *
     * @return void
     */
    public function __construct() {
        // $this->middleware('auth:api' );
    }

    /**
     * Get all shops
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function all() {
        $centers = CentroCommerciale::get();
        return response()->json( $centers );
    }
}
