<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
namespace App\Http\Controllers;

use App\Models\Catena;

class CatenaController extends Controller {

    /**
     * Create a new CatenaController instance.
     *
     * @return void
     */
    public function __construct() {
        // $this->middleware('auth:api' );
    }

    /**
     * Get all shops
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function all() {
        $centers = Catena::get();
        return response()->json( $centers );
    }
}
