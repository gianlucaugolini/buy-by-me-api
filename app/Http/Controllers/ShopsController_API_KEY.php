<?php

namespace App\Http\Controllers;

use App\Events\ShopCreatedEvent;
use App\Events\UserCreatedEvent;
use App\Events\UserShopCreatedEvent;
use App\Models\Commerciant;
use App\Models\Shop;
use App\Models\User;
use App\Models\Supervisor;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\ShopsController;


class ShopsController_API_KEY extends ShopsController {
    /**
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function createFull( Request $request ) {

        $request_data = $request->all();


        if (isset( $request_data[ 'api_key' ]) || $request->filled('api_key' )) { //controlla la presenza di api_key.

            $api_key = trim($request_data['api_key']);

            if (strlen($api_key)<8){
                return $this->errorResponse( 'api_key', Response::HTTP_NOT_ACCEPTABLE );
            }

            $API_KEYS = explode(";", settings()->get('enabled_api_keys', $default = [], $fresh = true));

            $found = false;

            foreach ($API_KEYS as $key) {

                if (trim($key) == $api_key){
                    $found = true;
                    break;
                }
            }

            if (!$found){
                return $this->errorResponse( 'api_key', Response::HTTP_UNAUTHORIZED );
            }
        }
        else {
            return $this->errorResponse( 'api_key', Response::HTTP_NOT_ACCEPTABLE );
        }

        return parent::createFull($request);
    }
}
