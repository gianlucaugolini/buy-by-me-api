<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
namespace App\Http\Controllers;
use App\Helpers\CommonHelpers;
use App\Helpers\PushNotificationsHelper;
use Illuminate\Support\Facades\Log;

class PushController extends Controller {

    /**
     * Create a new TestController instance.
     *
     * @return void
     */
    public function __construct() {
        // $this->middleware('auth:api' );
    }

    public function send($offer_id) {
       Log::debug(PushNotificationsHelper::sendPushNotificationForOfferWithID($offer_id));
    }

    public function sendDebug($offer_id) {
        Log::debug(PushNotificationsHelper::sendPushNotificationForOfferWithID($offer_id, true));
    }
}
