<?php

namespace App\Http\Controllers;

use App\Mail\SendFeedback;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use App\Traits\ApiResponser;

class FeedbacksController extends Controller {

    use ApiResponser;

    /**
     * Create a new TestController instance.
     *
     * @return void
     */
    public function __construct() {
        // $this->middleware('auth:api' );
    }

    public function send( Request $request )
    {
        $request_data = $request->all();

        $user = auth()->user();

        $validation_result = $this->sendFeedbackValidator($request_data);

        // se la validazione non ha successo
        if (!$validation_result['result']) {
            // restituisce gli errori di validazione con il codice HTTP 422 Unprocessable Entity
            return $this->errorResponse($validation_result['errors'], Response::HTTP_UNPROCESSABLE_ENTITY);
        } // se l'esecuzione procede

        $data = [
            'user' => $user,
            'feedback' => $request_data['feedback']
        ];


        //$admin_emails = User::byTypeId(User::ADMINISTRATOR)->get()->pluck('email')->toArray();

        $admin_emails = ["neogene@gmail.com","info@buybyme.net"];// explode(';', env("NOTIFICATION_EMAIL",'info@buybyme.net' ) );

        // $sent = Mail::to( $admin_emails )->send( new PurchaseOfferRequest( $data ) );
        $sent = Mail::to($admin_emails)->send(new SendFeedback($data));

        if (count(Mail::failures()) > 0) {
            return $this->errorResponse($validation_result['errors'], Response::HTTP_INTERNAL_SERVER_ERROR);
        } else {
            return $this->successResponse("OK", Response::HTTP_OK);
        }

    }

    private function sendFeedbackValidator( $data ) {
        $validationResult = [
            'result' => true,
            'errors' => []
        ];

        $offer_rules = [
            'feedback'        =>  'required'
        ];

        $offer_validation = Validator::make( $data, $offer_rules );

        if ( !$offer_validation->passes() ) {
            $validationResult = [
                'result' => false,
                'errors' => $offer_validation->messages()
            ];
        }


        return $validationResult;

    }
}
