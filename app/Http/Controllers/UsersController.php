<?php

namespace App\Http\Controllers;

use App\Events\ResetPasswordEvent;
use App\Models\Commerciant;
use App\Models\Customer;
use App\Models\User;
use App\Traits\ApiResponser;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Mail\ResetPasswordConfirm;
use Illuminate\Support\Facades\Mail;
use Log;

class UsersController extends Controller {
    use ApiResponser;
    /**
     *
     * @return void
     */
    public function __construct() {}

    public function activateAccount( $activation_token ) {

        try { // tenta l'inserimento dell'ordine e delle relative relazioni

            $user = User::byActivationToken( $activation_token )->firstOrFail();

            // inizializza un transazione
            DB::beginTransaction();

            $user->activation_token = null;
            $user->email_verified_at = Carbon::now();
            $user->save();

            if( $user->type_id === User::COMMERCIANT ) {
                $commerciant_details = Commerciant::byUserId( $user->id )->firstOrFail();
                $commerciant_details->c_confirmed = true;
                $commerciant_details->save();
            } elseif ( $user->type_id === User::CUSTOMER ) {
                $customer_details = Customer::byUserId( $user->id )->firstOrFail();
                $customer_details->u_confirmed = true;
                $customer_details->save();
            }

            // committa la transazione
            DB::commit();

            return view( 'auth.activation_ok' );

        } catch (\Exception $e) { // se genera eccezione

            return view( 'auth.activation_ko' );

        }

    }

    public function requestResetPassword( Request $request ) {
        $request_data = $request->all();

        $validation_result = $this->requestResetPasswordValidator( $request_data );

        // se la validazione non ha successo
        if( !$validation_result[ 'result' ] ) {
            // restituisce gli errori di validazione con il codice HTTP 422 Unprocessable Entity
            return $this->errorResponse( $validation_result[ 'errors' ], Response::HTTP_UNPROCESSABLE_ENTITY );
        } // se l'esecuzione procede

        try { // tenta l'inserimento dell'ordine e delle relative relazioni

            $user = User::where( [ 'email' => $request_data[ 'email' ] ] )->firstOrFail();

            // inizializza un transazione
            DB::beginTransaction();

            $user->api_password_reset_token =  str_random( 64 );
            $user->save();

            // committa la transazione
            DB::commit();

            event( new ResetPasswordEvent( $user ) );

            return $this->successResponse( "OK", Response::HTTP_OK );

        }
        catch(ModelNotFoundException $e) {
            // ritorna un messaggio di errore con codice 404 INTERNAL SERVER ERROR
            return $this->errorResponse( $e->getMessage(), Response::HTTP_NOT_FOUND);
        }
        catch (\Exception $e) { // se genera eccezione
            // ritorna un messaggio di errore con codice 500 INTERNAL SERVER ERROR
            return $this->errorResponse( $e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR );

        }

    }

    private function requestResetPasswordValidator( $data ) {

        $validationResult = [
            'result' => true,
            'errors' => []
        ];

        $user_rules = [
            'email'   =>   'required|exists:users',
        ];

        $user_validation = Validator::make( $data, $user_rules  );

        if ( !$user_validation->passes() ) {
            $validationResult = [
                'result' => false,
                'errors' => $user_validation->messages()
            ];
        }


        return $validationResult;
    }

    public function resetPassword( $reset_token ) {
        $user = User::byApiPasswordResetToken( $reset_token )->first();

        if ( !$user ||  $user->type_id === User::ADMINISTRATOR ) {
            return view('auth.reset_password_ko',["reason"=>"Nessuna corrispondenza nella base dati"]);
        }

        return view( 'auth.reset-password-form' );

    }

    public function saveNewPassword( Request $request ) {

        $request_data = $request->all();

        $validation_result = $this->requestSavePasswordValidator( $request_data );

        if( !$validation_result[ 'result' ] ) {
            // restituisce gli errori di validazione con il codice HTTP 422 Unprocessable Entity
            return view('auth.reset_password_ko',["reason"=>$validation_result[ 'errors' ]->first()]);
        } // se l'esecuzione procede


        try { // tenta l'inserimento dell'ordine e delle relative relazioni

            $user = User::byApiPasswordResetToken( $request_data[ 'activation_token' ] )->first();
            if ( !$user ) {
                return view('auth.reset_password_ko',["reason"=>"Non è stato possibile trovare un utente associato al token di reset password fornito"]);
            }

            $password = $request_data[ 'new_password' ];

            // inizializza un transazione
            DB::beginTransaction();

            $user->api_password_reset_token = null;
            $user->password = bcrypt( $password );
            $user->save();

            // committa la transazione
            DB::commit();

            //Invia email
            Mail::to( [ $user->email ] )->send( new ResetPasswordConfirm( $user, $password ) );

            //Log::debug("CONFIRM RESET EMAIL SENT TO: ".$user->email . var_dump($request_data));

            return view('auth.reset_password_ok',["reason"=>"Reset effettuato per ".$user->email]);

        } catch (\Exception $e) { // se genera eccezione

            return view('auth.reset_password_ko',["reason"=>"Errore del server"]);

        }

    }

    private function requestSavePasswordValidator( $data ) {

        $validationResult = [
            'result' => true,
            'errors' => []
        ];

        $user_rules = [
            'activation_token' => 'bail|required|string',
            'new_password'   =>   'required_with:new_password_confirmation|string|min:8|max:128',
        ];

        $user_validation = Validator::make( $data, $user_rules  );

        if ( !$user_validation->passes() ) {
            $validationResult = [
                'result' => false,
                'errors' => $user_validation->messages()
            ];
        }


        return $validationResult;
    }
}
