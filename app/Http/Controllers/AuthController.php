<?php

namespace App\Http\Controllers;

use App\Models\Commerciant;
use App\Models\Customer;
use App\Models\Supervisor;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller {
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth:api', [ 'except' => [ 'login', 'authenticate', 'register' ] ] );
    }

    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login( Request $request ) {

        $credentials = request( [ 'email', 'password' ] );

        $user = $this->userExists( $credentials );

        if( $user ) {

            if( $this->checkBlocked( $user ) ) {
                return response()->json( [ 'error' => 'Utente bloccato' ], 400 );
            }
            else  if( !($this->checkAuthorized( $user ))) {
                return response()->json( [ 'error' => 'È necessario autorizzare l\'account tramite la mail di conferma'], 400 );
            }

            try {

                if ( !$token = auth()->login( $user ) ) {

                    return response()->json( [ 'error' => 'Nome utente o email o password non validi' ], 400 );

                }

            } catch ( JWTException $e ) {

                return response()->json( [ 'error' => 'Impossibile creare un token' ], 500 );

            }

            $user->update( [ 'logged' => 1 ] );

            return $this->respondWithToken( $token );

        }

        return response()->json( [ 'error' => 'Nome utente o email o password non validi' ], 400 );


    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me() {

        $user = auth()->user();

        if( $user ) {

            if( $user->type_id === User::CUSTOMER ) {
                $user->details = Customer::with( [
                    'favs' => function( $q ) {
                        $q->onlyId();
                    },
                    'likedOffers' => function( $q ) {
                        $q->onlyId();
                    }
                ] )
                    ->byUserId( $user->id )
                    ->firstOrFail();

                $favs_only_ids = $user->details->favs->pluck( 'id' );
                unset( $user->details->favs );
                $user->details->favs = $favs_only_ids;

                $likes_only_ids = $user->details->likedOffers->pluck( 'id' );
                unset( $user->details->likedOffers );
                $user->details->likes = $likes_only_ids;

            } else if( $user->type_id === User::SUPERVISOR ) {

                $user->details = Supervisor::with( [
                    'catena' => function( $q ) {
                        $q->with( [
                            'shops' => function ( $q ) {
                                $q
                                    //->notBlocked()
                                    ->with(
                                        [
                                            'owner',
                                            'offerte' => function( $q ) {
                                               // $q->notBlocked();
                                            }
                                        ] );
                            }
                        ] );
                    }
                ] )->byUserId( $user->id )->firstOrFail();

            } else if( $user->type_id === User::COMMERCIANT ) {

                $user->details = Commerciant::with( [
                    'shop' => function( $q ) {
                       // $q->notBlocked();
                        /*$q->with( [
                            'offerte' => function( $q ) {
                               // $q->notBlocked();
                            }
                        ] );*/
                    }
                ] )->byUserId( $user->id )->firstOrFail();

            } else {

                $user->details = null;

            }

        }

        return response()->json( $user );

    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout() {

        $user = auth()->user();

        if( $user ) {
            $user->update( [ 'logged' => 0 ] );
        }

        auth()->logout();

        return response()->json( [ 'message' => 'Successfully logged out' ] );

    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh() {

        return $this->respondWithToken( auth()->refresh() );

    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken( $token ) {
        return response()->json( [
            'access_token'  => $token,
            'token_type'    => 'bearer',
            'expires_in'    => auth()->factory()->getTTL() * 60
        ] );
    }

    protected function checkBlocked( $user ) {

        switch ( $user->type_id ) {
            case User::CUSTOMER :
                $blocked = Customer::byUserId( $user->id )->first()->u_blocked;
                break;
            case User::COMMERCIANT :
                $blocked = Commerciant::byUserId( $user->id )->first()->c_blocked;
                break;
            case User::SUPERVISOR :
                $blocked = Supervisor::byUserId( $user->id )->first()->ccs_blocked;
                break;
            default:
                $blocked = true;
        }

        return $blocked;
    }

    protected function checkAuthorized( $user ) {

        switch ( $user->type_id ) {
            case User::CUSTOMER :
                $authorized = !is_null( $user->email_verified_at );
                break;
            case User::COMMERCIANT :
                $authorized = !is_null( $user->email_verified_at );
                break;
            case User::SUPERVISOR :
                $authorized = !is_null( $user->email_verified_at );
                break;
            default:
                $authorized = false;
        }

        return $authorized;
    }

    protected function userExists( $credentials ) {
        $user = User::where( [ 'email' => $credentials[ 'email' ] ] )->first();

        if( $user ) {
            $pwd_check = Hash::check(  $credentials[ 'password' ], $user->password );

            if( $pwd_check ) {
                return $user;
            }
        }

        return null;
    }

    public function getAuthenticatedUser() {

        try {

            if ( !$user = JWTAuth::parseToken()->authenticate() ) {

                return response()->json(['user_not_found'], 404);

            }

        } catch ( Tymon\JWTAuth\Exceptions\TokenExpiredException $e ) {

            return response()->json(['token_expired'], $e->getStatusCode() );

        } catch ( Tymon\JWTAuth\Exceptions\TokenInvalidException $e ) {

            return response()->json(['token_invalid'], $e->getStatusCode() );

        } catch ( Tymon\JWTAuth\Exceptions\JWTException $e ) {

            return response()->json(['token_absent'], $e->getStatusCode() );

        }

        return response()->json( compact('user' ) );
    }

    public function register( Request $request ) {

        $validator = Validator::make( $request->all(), [
            'fullname'  => 'required|string|max:255',
            'email'     => 'required|string|email|max:255|unique:users,email',
            'password'  => 'required|string|min:6',
            'type'      => 'required|numeric|in:1,2,3,4',
        ] );

        if ( $validator->fails() ){
            return response()->json( $validator->errors()->toJson(), 400 );
        }

        $user = User::create( [
            'fullname'  => $request->get( 'fullname' ),
            'email'     => $request->get( 'email' ),
            'password'  => Hash::make( $request->get( 'password' ) ),
            'type_id'   => $request->get( 'type_id' ),
        ]);

        $token = JWTAuth::attempt( request( [ 'email', 'password' ] ) );

        return response()->json( compact( 'user','token' ),201 );
    }
}
