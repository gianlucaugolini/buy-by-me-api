<?php

namespace App\Http\Controllers;

use App\Events\ShopCreatedEvent;
use App\Events\ShopUpdatedEvent;
use App\Events\UserCreatedEvent;
use App\Events\UserShopCreatedEvent;
use App\Helpers\CommonHelpers;
use App\Models\Commerciant;
use App\Models\Shop;
use App\Models\User;
use App\Models\Supervisor;
use App\Traits\ApiResponser;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;


class ShopsController extends Controller {

    use ApiResponser;

    /**
     * Create a new ShopsController instance.
     *
     * @return void
     */
    public function __construct() {
        // $this->middleware('auth:api' );
    }

    /**
     * Get all shops
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function all() {
        try {
            $user = auth()->user();

            switch ($user->type_id) {
                case User::COMMERCIANT:
                    return $this->all_commerciant();
                    break;
                case User::SUPERVISOR:
                    return $this->all_supervisor();
                    break;
                default: //user

                    $showShopsWithNoOffers = settings()->get('show_empty_shops',$default = null, $fresh = true);

                    $shops = null;

                    if (!empty($showShopsWithNoOffers)){

                        //return $this->errorResponse(settings()->get('show_empty_shops', $default = 'empty', $fresh = true), Response::HTTP_INTERNAL_SERVER_ERROR );

                        $shops = Shop::notBlocked()

                            ->whereHas( 'owner', function( $q ) {
                                $q->subscriptionActive();
                            })
                            ->with(
                                [
                                    'offerte' => function ($q) {
                                        $q->notBlockedAndNotCancelled()->orderBy('id', 'DESC');
                                    },
                                    'centroCommerciale' => function ($q) {
                                        $q->select( 'id', 'cc_name' );
                                    },
                                    'owner' => function( $q ) {
                                        $q->subscriptionActive();
                                    }
                                ]
                            )->get();
                     }
                    else {

                        $shops = Shop::whereHas(
                            'offerte', function ($query) {
                                $query->notBlockedAndNotCancelled()->orderBy('id', 'DESC');
                            })
                            ->whereHas( 'owner', function( $q ) {
                                $q->subscriptionActive();
                            })
                            ->with(
                                [
                                    'offerte',
                                    'centroCommerciale' => function ($q) {
                                        $q->select( 'id', 'cc_name' );
                                    },
                                    'owner'
                                ])->get();
                    }

                    // $shops = $shops->except(['o_views']);

                    //$shops["type"] = "customer";

                    return response()->json($shops);

                    break;
            }
        }
        catch (\Exception $e) { // se genera eccezione
            // ritorna un messaggio di errore con codice 500 INTERNAL SERVER ERROR
            return $this->errorResponse( $e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR );

        }

    }

    function all_supervisor() {

        $user = auth()->user();

        if( $user->type_id !== User::SUPERVISOR ) {
            // l'utente non è un commerciant HTTP 401 Unprocessable Entity
            return $this->errorResponse( 'Unauthorized', Response::HTTP_UNAUTHORIZED );
        }

        $supervisor = Supervisor::where('user_id', $user->id)->firstOrFail();

        $shops = Shop::with(
            [
                'offerte' => function( $q ) {
                    $q->notBlocked()->orderBy('id', 'DESC');
                }, 'catena'
            ] )->where('s_catena_commerciale_ext',$supervisor->cc_ext)->get();

        //$shops["type"] = "supervisor";

        return response()->json( $shops );
    }

    function all_commerciant() {

        $user = auth()->user();

        if( $user->type_id !== User::COMMERCIANT ) {
            // l'utente non è un commerciant HTTP 401 Unprocessable Entity
            return $this->errorResponse( 'Unauthorized', Response::HTTP_UNAUTHORIZED );
        }

        $commerciant = Commerciant::where( 'user_id', $user->id )->with( 'shop' )->firstOrFail();

        if (!is_null($commerciant->c_subscription) && Carbon::createFromTimestamp($commerciant->c_subscription)->isPast()){
            return $this->errorResponse(  'Attenzione: subscription scaduta, contattaci per richiedere chiarimenti.', Response::HTTP_UNPROCESSABLE_ENTITY );
        }

        $shops = Shop::with(
            [
                'offerte' => function( $q ) {
                    $q->notBlocked()->orderBy('id', 'DESC');
                }
            ] )->where('s_commerciant_ext',$commerciant->id)->get();

        //$shops["type"] = "commerciant";

        return response()->json( $shops );
    }

    /**
     * Get Shop by ID
     *
     * @return \Illuminate\Http\JsonResponseF
     */
    public function details( $shop_id ) {
        $shop = Shop::notBlocked()->with(
            [
                'offerte',
                'owner'
            ] )->findOrFail( $shop_id );

        if (!is_null($shop->owner->c_subscription) && Carbon::createFromTimestamp($shop->owner->c_subscription)->isPast()){
            return $this->errorResponse(  'Attenzione: subscription scaduta, contattaci per richiedere chiarimenti.', Response::HTTP_UNPROCESSABLE_ENTITY );
        }

        return response()->json( $shop );

        /*
         * $shops = Cache::remember('shops', 60*24*360, function() use ($shop_id) { //oneyear cache
            return Shop::notBlocked()->with(
                [
                    'offerte',
                    'owner'
                ] )->findOrFail( $shop_id );
        });
        return response()->json($shops);
         */
    }


    /**
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function createFull( Request $request ) {

        $request_data = $request->all();

        $validation_result = $this->createShopFullValidator( $request_data );

        // se la validazione non ha successo
        if( !$validation_result[ 'result' ] ) {
            // restituisce gli errori di validazione con il codice HTTP 422 Unprocessable Entity
            return $this->errorResponse( $validation_result[ 'errors' ], Response::HTTP_UNPROCESSABLE_ENTITY );
        } // se l'esecuzione procede


        try { // tenta l'inserimento dell'ordine e delle relative relazioni

            // inizializza un transazione
            DB::beginTransaction();

            $user = new User;

            $user->fullname = $request_data[ 'c_name' ] . ' ' . $request_data[ 'c_last_name' ];
            $user->password = bcrypt( $request_data[ 'password' ] );
            $user->email = $request_data[ 'email' ];
            $user->type_id = User::COMMERCIANT;
            $user->activation_token = str_random( 64 );
            $user->api_id =  isset( $request_data[ 'api_key' ]) && strlen(trim($request_data[ 'api_key' ]))>0 ? trim($request_data[ 'api_key' ]) : null;
            $user->save();

            $commerciant_details = new Commerciant;


            $commerciant_details->c_name = $request_data[ 'c_name' ];
            $commerciant_details->c_last_name = $request_data[ 'c_last_name' ];
            $commerciant_details->user_id = $user->id;
            $commerciant_details->c_offers_bought = settings()->get('default_commerciant_coupons',$default = 0, $fresh = true);
            $commerciant_details->api_id = isset( $request_data[ 'api_key' ]) && strlen(trim($request_data[ 'api_key' ]))>0 ? trim($request_data[ 'api_key' ]) : null;

            $commerciant_details->save();

            $shop = new Shop;

            $shop->s_commerciant_ext            = $commerciant_details->id;
            $shop->s_name                       = $request_data[ 's_name' ];
            $shop->s_desc                       = $request_data[ 's_desc' ];
            $shop->s_phone                      = isset($request_data[ 's_phone' ]) ? $request_data[ 's_phone' ] : null;
            $shop->s_web                        = isset($request_data[ 's_web' ]) ? $request_data[ 's_web' ] : '';
            $shop->s_prd                        = $request_data[ 's_prd' ];
            $shop->s_address                    = $request_data[ 's_address' ];
            $shop->s_cap                        = $request_data[ 's_cap' ];
            $shop->s_city                       = ucfirst(strtolower($request_data[ 's_city' ]));
            $shop->s_prov                       = $request_data[ 's_prov' ];
            $shop->s_prov_code                  = strtoupper($request_data[ 's_prov_code' ]);
            $shop->s_lat                        = isset( $request_data[ 's_lat' ] ) ? $request_data[ 's_lat' ] : null;
            $shop->s_lon                        = isset( $request_data[ 's_lon' ] ) ? $request_data[ 's_lon' ] : null;
            $shop->s_cat                        = isset( $request_data[ 's_cat' ] ) ? $request_data[ 's_cat' ] : 1;
            $shop->s_catena_commerciale_ext     = isset( $request_data[ 's_catena_commerciale_ext' ] ) ? $request_data[ 's_catena_commerciale_ext' ]: 0;
            $shop->s_centro_commerciale_ext     = isset( $request_data[ 's_centro_commerciale_ext' ] ) ? $request_data[ 's_centro_commerciale_ext' ]: 0;
            $shop->s_centro_commerciale_piano   = isset( $request_data[ 's_centro_commerciale_piano' ] ) ? $request_data[ 's_centro_commerciale_piano' ]: null;
            $shop->api_id                       = isset( $request_data[ 'api_key' ]) && strlen(trim($request_data[ 'api_key' ]))>0 ? trim($request_data[ 'api_key' ]) : null;

            //todo NOTE: FREE LIMIT IS 2500/day https://developers.google.com/maps/previous-pricing
            if (!isset( $request_data[ 's_lat' ] ) && !isset( $request_data[ 's_lon' ] )) {
                $address = urlencode($shop->s_address." ".$shop->s_city." ");
                $url = "https://maps.google.com/maps/api/geocode/json?address={$address}&sensor=false&key=AIzaSyBY7SxsSlKOVTHBQr9GpSTcSpvJ-8saBdw";//we are getting the response in json
                $resp_json = file_get_contents($url);
                $resp = json_decode($resp_json, true);
                if ($resp['status'] == 'OK') {
                    $shop->s_lat = $resp['results'][0]['geometry']['location']['lat'];
                    $shop->s_lon = $resp['results'][0]['geometry']['location']['lng'];
                }
                else {
                    dd("Contacted:$url -> ".var_dump($resp));
                }
            }

            $shop->save();


            if ( $request->hasFile('s_image') ) {

                $image = $request->file( 's_image' );

                $name = $shop->id . '.' . $image->getClientOriginalExtension();

                $image->storeAs('public/shop-images', $name );

                $shop->s_image = 'storage/shop-images/' . $name;

            }

            // la salva
            $shop->save();

            // committa la transazione
            DB::commit();

            event( new ShopCreatedEvent( $shop, $commerciant_details,$request->filled('debug') ) );
            event( new UserShopCreatedEvent( $user ) );
            // ritorna codice 201 CREATED
            return $this->successResponse("OK",Response::HTTP_CREATED);
            //return $this->showAggregatedResult( collect( [ $user, $commerciant_details, $shop ] ),Response::HTTP_CREATED );

        } catch (\Exception $e) { // se genera eccezione

            if (strpos(strtolower($e->getMessage()),"email")!==false){
                return $this->errorResponse( "email", Response::HTTP_INTERNAL_SERVER_ERROR );
            }
            // ritorna un messaggio di errore con codice 500 INTERNAL SERVER ERROR
            return $this->errorResponse( $e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR );

        }
    }

    private function createShopFullValidator( $data ) {

        $validationResult = [
            'result' => true,
            'errors' => []
        ];

        $offer_rules = [
            'c_name'                        =>  'required|max:255',
            'c_last_name'                   =>  'required|min:2|max:255',
            'email'                         =>  'required|email|unique:users|max:255',
            'password'                      =>  'required|min:8|max:255',
            's_name'                        =>  'required|min:2|max:128',
            's_desc'                        =>  'required|min:3|max:1024',
            's_phone'                       =>  'nullable|max:24',
            's_web'                         =>  'nullable|url|max:64',
            's_prd'                         =>  'max:255',
            's_address'                     =>  'required|min:2|max:128',
            's_cap'                         =>  'nullable|regex:/^[0-9]{5}$/','size:5',
            's_city'                        =>  'required|min:2|max:128',
            's_prov'                        =>  'required|min:2|max:50',
            's_prov_code'                   =>  'required|size:2|alpha',
            's_lat'                         =>  'max:16',
            's_lon'                         =>  'max:16',
            's_cat'                         =>  'numeric|exists:category,id',
            's_catena_commerciale_ext'      =>  'nullable|numeric|exists:catena_commerciale,id',
            's_centro_commerciale_ext'      =>  'nullable|numeric|exists:centro_commerciale,id',
            's_centro_commerciale_piano'    =>  'max:32',
            's_image'                       =>  'nullable|file|max:8192|mimes:jpeg,jpg,png',
        ];

        $messages = array(
            'c_name.required' => 'Nome mancante',
            'c_last_name.required' => 'Cognome mancante',
            'email.required' => 'Email mancante',
            'email.unique' => 'Email già utilizzata',
            'password.required' => 'Password mancante',
            's_address.required' => 'Indirizzo mancante',
            's_name.required' => "Nome negozio mancante",
            's_city.required' => "Città mancante",
            's_city.max' => "Non è possibile inserire una città con più di 128 caratteri",
            's_prov.required' => "Provincia mancante",
            's_prov_code.required' => "Codice provincia mancante",
            'email.email' => 'Email non valida',
            's_desc.required'=> 'Descrizione mancante',
            's_desc.min'=> 'Descrizione troppo breve',
            's_image.mimes' => 'Formato immagine non valido',
        );

        $offer_validation = Validator::make( $data, $offer_rules, $messages );

        if ( !$offer_validation->passes() ) {
            $validationResult = [
                'result' => false,
                'errors' => $offer_validation->messages()
            ];
        }


        return $validationResult;
    }



    /**
     *
     *
     * @return \Illuminate\Http\Response
     */
    public function edit( Request $request ) {

        $request_data = $request->all();

        $user = auth()->user();

        if( $user->type_id !== User::COMMERCIANT ) {
            // l'utente non è un commerciant HTTP 401 Unprocessable Entity
            return $this->errorResponse( 'Unauthorized', Response::HTTP_UNAUTHORIZED );
        }

        $commerciant = Commerciant::byUserId( $user->id )
            ->with( [
                'shop' => function( $q ) {
                    $q->with(
                        [
                            'categoria',
                            'catena',
                            'centroCommerciale'
                        ]
                    );
                }
            ] )
            ->firstOrFail();

        $validation_result = $this->editShopValidator( $request_data, $user->id );

        // se la validazione non ha successo
        if( !$validation_result[ 'result' ] ) {
            // restituisce gli errori di validazione con il codice HTTP 422 Unprocessable Entity
            return $this->errorResponse( $validation_result[ 'errors' ], Response::HTTP_UNPROCESSABLE_ENTITY );
        } // se l'esecuzione procede

        $shop = $commerciant->shop;
        if( intval( $request_data[ 'shop_id' ] ) !== $shop->id ) {
            // l'utente non è un commerciant HTTP 401 Unprocessable Entity
            return $this->errorResponse( 'Unauthorized', Response::HTTP_UNAUTHORIZED );
        }

        try { // tenta l'inserimento dell'ordine e delle relative relazioni

            $mail_data = [
                'old_user'          => $user->replicate(),
                'old_commerciant'   => $commerciant->replicate(),
                'old_shop'          => $shop->replicate()
            ];

            // inizializza un transazione
            DB::beginTransaction();

            if( $request_data[ 'c_name' ] !== '' &&  $request_data[ 'c_last_name' ] !== '' ) {

                $commerciant->c_name = $request_data[ 'c_name' ];
                $commerciant->c_last_name = $request_data[ 'c_last_name' ];
                $user->fullname = $request_data[ 'c_name' ] . ' ' . $request_data[ 'c_last_name' ];

                if( $request_data[ 'email' ] !== '' ) {

                    $user->email = $request_data[ 'email' ];

                }

                $user->save();
                $commerciant->save();
            }

            $shop->s_name                       = $request_data[ 's_name' ];
            $shop->s_desc                       = $request_data[ 's_desc' ];
            $shop->s_phone                      = isset($request_data[ 's_phone' ]) ? $request_data[ 's_phone' ] : null;
            $shop->s_web                        = isset($request_data[ 's_web' ]) ? $request_data[ 's_web' ] : '';
            $shop->s_prd                        = $request_data[ 's_prd' ];
            $shop->s_address                    = $request_data[ 's_address' ];
            $shop->s_cap                        = $request_data[ 's_cap' ];
            $shop->s_city                       = ucfirst(strtolower($request_data[ 's_city' ]));
            $shop->s_prov                       = $request_data[ 's_prov' ];
            $shop->s_prov_code                  = strtoupper($request_data[ 's_prov_code' ]);
            if (isset( $request_data[ 's_lat' ] ))  $shop->s_lat = $request_data[ 's_lat' ];
            if (isset( $request_data[ 's_lon' ] ))  $shop->s_lon = $request_data[ 's_lon' ];
            $shop->s_cat                        = isset( $request_data[ 's_cat' ] ) ? $request_data[ 's_cat' ] : 1;
            $shop->s_catena_commerciale_ext     = isset( $request_data[ 's_catena_commerciale_ext' ] ) ? $request_data[ 's_catena_commerciale_ext' ]: 0;
            $shop->s_centro_commerciale_ext     = isset( $request_data[ 's_centro_commerciale_ext' ] ) ? $request_data[ 's_centro_commerciale_ext' ]: 0;
            $shop->s_centro_commerciale_piano   = isset( $request_data[ 's_centro_commerciale_piano' ] ) ? $request_data[ 's_centro_commerciale_piano' ]: null;

            //2500/day
            if (!isset( $request_data[ 's_lat' ] ) && !isset( $request_data[ 's_lon' ] )) {
                $address = urlencode($shop->s_address." ".$shop->s_city." ");
                $url = "https://maps.google.com/maps/api/geocode/json?address={$address}&sensor=false&key=AIzaSyBY7SxsSlKOVTHBQr9GpSTcSpvJ-8saBdw";//we are getting the response in json
                $resp_json = file_get_contents($url);
                $resp = json_decode($resp_json, true);
                if ($resp['status'] == 'OK') {
                    $shop->s_lat = $resp['results'][0]['geometry']['location']['lat'];
                    $shop->s_lon = $resp['results'][0]['geometry']['location']['lng'];
                }
                else {
                    dd("Contacted:$url -> ".var_dump($resp));
                }
            }


            if ( $request->hasFile('s_image') ) { //overrides if added

                $image = $request->file( 's_image' );

                $name = $shop->id . '.' . $image->getClientOriginalExtension();

                $image->storeAs('public/shop-images', $name );

                $shop->s_image = 'storage/shop-images/' . $name;

            }

            // la salva
            $shop->save();

            // committa la transazione
            DB::commit();

            event( new ShopUpdatedEvent( $mail_data, $shop, $commerciant, $user, $request->filled('debug') ) );

            // ritorna l'offerta con un codice 200 OK
            return $this->showOne( $shop,Response::HTTP_OK );

        } catch (\Exception $e) { // se genera eccezione

            // ritorna un messaggio di errore con codice 500 INTERNAL SERVER ERROR
            return $this->errorResponse( $e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR );

        }

    }

    private function editShopValidator( $data, $user_id ) {
        $validationResult = [
            'result' => true,
            'errors' => []
        ];

        $offer_rules = [
            'c_name'                        =>  'max:255',
            'c_last_name'                   =>  'min:2|max:255',
            'email'                         =>  'max:255|email|unique:users,email,' . $user_id . ',id',
            'shop_id'                       =>  'required|numeric',
            's_name'                        =>  'required|min:2|max:128',
            's_desc'                        =>  'required|min:3|max:1024',
            's_phone'                       =>  'nullable|max:24',
            's_web'                         =>  'nullable|url|max:64',
            's_prd'                         =>  'max:255',
            's_address'                     =>  'required|min:2|max:128',
            's_cap'                         =>  'nullable|regex:/^[0-9]{5}$/','size:5',
            's_city'                        =>  'required|min:2|max:128',
            's_prov'                        =>  'required|min:2|max:50',
            's_prov_code'                   =>  'required|size:2|alpha',
            's_lat'                         =>  'max:16',
            's_lon'                         =>  'max:16',
            's_cat'                         =>  'numeric|exists:category,id',
            's_catena_commerciale_ext'      =>  'nullable|numeric|exists:catena_commerciale,id',
            's_centro_commerciale_ext'      =>  'nullable|numeric|exists:centro_commerciale,id',
            's_centro_commerciale_piano'    =>  'max:32',
            's_image'                       =>  'nullable|file|max:1024|mimes:jpeg,jpg,png',
        ];

        $offer_validation = Validator::make( $data, $offer_rules );

        if ( !$offer_validation->passes() ) {
            $validationResult = [
                'result' => false,
                'errors' => $offer_validation->messages()
            ];
        }


        return $validationResult;

    }
}
