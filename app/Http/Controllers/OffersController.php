<?php

namespace App\Http\Controllers;

use App\Events\OfferCreatedEvent;
use App\Http\Requests\AddOfferRequest;
use App\Http\Requests\EditOfferRequest;
use App\Mail\PurchaseOfferRequest;
use App\Mail\SubscriptionRenewalRequest;
use App\Models\Category;
use App\Models\Commerciant;
use App\Models\Offer;
use App\Models\Supervisor;
use App\Models\User;
use App\Traits\ApiResponser;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class OffersController extends Controller {

    use ApiResponser;
    /**
     * Create a new OffersController instance.
     *
     * @return void
     */
    public function __construct() {
        // $this->middleware('auth:api' );
    }

    /**
     * Get all shops
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function all() {

        $offers = Offer::notBlocked()->with(
            [
                'shop' => function( $q ) {
                    $q->with( [
                        'owner',
                        'catena'
                    ] );
                }
            ] )->get();
        return response()->json( $offers );
    }

    /**
     * Get Shop by ID
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function details( $offer_id ) {
        $offer = Offer::notBlocked()->with(
            [
                'favedBy',
                'shop'      => function( $q ) {
                    $q->with( [
                        'owner',
                        'catena'
                    ] );
                }
            ] )->findOrFail( $offer_id );



        if (!is_null($offer->shop->owner->c_subscription) && Carbon::createFromTimestamp($offer->shop->owner->c_subscription)->isPast()){
            return $this->errorResponse(  'Attenzione: subscription scaduta, contattaci per richiedere chiarimenti.', Response::HTTP_UNPROCESSABLE_ENTITY );
        }

        return response()->json( $offer );
    }

    /**
     * Get Shop by ID
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function setViews( Request $request ) {

        $request_data = $request->all();

        $offers_viewed = [];

        if (is_array($request_data[ 'offers' ])) {
            $offers_viewed = $request_data['offers'];
        }
        else {
            $request_data['offers'] = str_replace("[","",$request_data['offers']);
            $request_data['offers'] = str_replace("]","",$request_data['offers']);
            $request_data['offers'] = str_replace(" ","",$request_data['offers']);

            if (strlen($request_data['offers'])>0) {
                //try splitting ,

                $offers_viewed = explode(",", $request_data['offers']);

                if (count($offers_viewed) == 0) {
                    //try splitting ;
                    $offers_viewed = explode(";", $request_data['offers']);
                }
            }
        }

        if (count($offers_viewed)>0) {
            $updated = Offer::whereIn('id', $offers_viewed)->update([
                'o_views' => DB::raw('o_views+1')
            ]);

            return response()->json([ 'result' => $updated ]);
        }

        return response()->json([ '' => '' ]);
    }

    /**
     * Nuova Offerta
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function new( Request $request ) {

        $request_data = $request->all();

        $user = auth()->user();

        if( $user->type_id !== User::COMMERCIANT ) {
            // l'utente non è un commerciant HTTP 401 Unprocessable Entity
            return $this->errorResponse( 'Unauthorized', Response::HTTP_UNAUTHORIZED );
        }

        $commerciant = Commerciant::byUserId( $user->id )->with( 'shop' )->firstOrFail();

        if ($commerciant->c_blocked){
            return $this->errorResponse(  'Attenzione: questo account è bloccato, contattaci per richiedere chiarimenti.', Response::HTTP_UNPROCESSABLE_ENTITY );
        }

        if (!is_null($commerciant->c_subscription) && Carbon::createFromTimestamp($commerciant->c_subscription)->isPast()){
            return $this->errorResponse(  'Attenzione: subscription scaduta, contattaci per richiedere chiarimenti.', Response::HTTP_UNPROCESSABLE_ENTITY );
        }


        $validation_result = $this->insertNewOfferValidator( $request_data );

        // se la validazione non ha successo
        if( !$validation_result[ 'result' ] ) {
            // restituisce gli errori di validazione con il codice HTTP 422 Unprocessable Entity
            return $this->errorResponse( $validation_result[ 'errors' ], Response::HTTP_UNPROCESSABLE_ENTITY );
        } // se l'esecuzione procede

        try { // tenta l'inserimento dell'ordine e delle relative relazioni

            date_default_timezone_set('Europe/Paris');

            // inizializza un transazione
            DB::beginTransaction();

            // istanzia una nuova Offerta
            $offer = new Offer();

            // inserisce le proprietà
            $offer->o_blocked = ($request->filled('test') && $request_data["test"] == 1) ? 1 : 0;
            $offer->o_title = $request_data[ 'o_title' ];
            $offer->o_desc = $request_data[ 'o_desc' ];
            $offer->o_pric_start =  $request->filled('o_pric_start') ? $request_data[ 'o_pric_start' ] : $request_data[ 'o_pric' ];
            $offer->o_pric = $request_data[ 'o_pric' ];
            $offer->o_disc = $request_data[ 'o_disc' ];
            $offer->o_howm = $request_data[ 'o_howm' ];
            $offer->o_start = $request_data[ 'o_start' ]; //yyyy-MM-dd HH:mm:ss
            $offer->o_end = $request_data[ 'o_end' ]; //yyyy-MM-dd HH:mm:ss
            $offer->o_cat = $request_data[ 'o_cat' ];
            $offer->o_shop_ext = $commerciant->shop->id;
            $offer->o_views = 0;

            // la salva
            $offer->save();

            //genera unique uid = shopid_offerid
            $offer->o_uid = $commerciant->shop->id."_". $offer->id;

            if ( $request->hasFile('o_image') ) {

                $image = $request->file( 'o_image' );

                $name = $offer->id . '.' . $image->getClientOriginalExtension();

                $image->storeAs('public/offer-images', $name );

                $offer->o_image = 'storage/offer-images/' . $name;

            }

            // la salva
            $offer->save();

            //incrementa le offerte utilizzate
            $commerciant->c_offers_used +=1;
            $commerciant->save();

            // committa la transazione
            DB::commit();

            $offer->o_uid = $offer->o_shop_ext . '_' . $offer->id; //not updates!

            //Fire event
           // if (!$request->filled('test') || $request_data["test"] != 1) {
                event(new OfferCreatedEvent($offer, $commerciant, $request->filled('debug')));
           // }

            // ritorna il codice 201 CREATED
            return $this->successResponse( "OK",Response::HTTP_CREATED );
            //return $this->showOne( $offer,Response::HTTP_CREATED );

        } catch (\Exception $e) { // se genera eccezione

            // ritorna un messaggio di errore con codice 500 INTERNAL SERVER ERROR
            return $this->errorResponse( $e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR );

        }

    }

    /**
     *
     *
     * @return \Illuminate\Http\Response
     */
    public function edit( Request $request ) {

        $request_data = $request->all();

        $user = auth()->user();

        if( $user->type_id !== User::COMMERCIANT ) {
            // l'utente non è un commerciant HTTP 401 Unprocessable Entity
            return $this->errorResponse( 'Unauthorized', Response::HTTP_UNAUTHORIZED );
        }

        $commerciant = Commerciant::byUserId( $user->id )->with( 'shop' )->firstOrFail();

        $validation_result = $this->editOfferValidator( $request_data );

        // se la validazione non ha successo
        if( !$validation_result[ 'result' ] ) {
            // restituisce gli errori di validazione con il codice HTTP 422 Unprocessable Entity
            return $this->errorResponse( $validation_result[ 'errors' ], Response::HTTP_UNPROCESSABLE_ENTITY );
        } // se l'esecuzione procede

        $offer = Offer::findOrFail( $request_data[ 'o_id' ] );

        if( $offer->o_shop_ext !== $commerciant->shop->id ) {
            // l'utente non è un commerciant HTTP 401 Unprocessable Entity
            return $this->errorResponse( 'Unauthorized', Response::HTTP_UNAUTHORIZED );
        }

        try { // tenta l'inserimento dell'ordine e delle relative relazioni

            // inizializza un transazione
            DB::beginTransaction();

            // inserisce le proprietà
            $offer->o_title = $request_data[ 'o_title' ];
            $offer->o_desc = $request_data[ 'o_desc' ];
            $offer->o_pric = $request_data[ 'o_pric' ];
            $offer->o_disc = $request_data[ 'o_disc' ];
            $offer->o_howm = $request_data[ 'o_howm' ];
            $offer->o_start = $request_data[ 'o_start' ];
            $offer->o_end = $request_data[ 'o_end' ];
            $offer->o_cat = $request_data[ 'o_cat' ];

            if ( $request->hasFile('o_image') ) {

                $image = $request->file( 'o_image' );

                $name = $offer->id . '.' . $image->getClientOriginalExtension();

                $image->storeAs('public/offer-images', $name );

                $offer->o_image = 'storage/offer-images/' . $name;

            }

            // la salva
            $offer->save();

            // committa la transazione
            DB::commit();

            // ritorna l'offerta con un codice 200 OK
            return $this->showOne( $offer,Response::HTTP_OK );

        } catch (\Exception $e) { // se genera eccezione

            // ritorna un messaggio di errore con codice 500 INTERNAL SERVER ERROR
            return $this->errorResponse( $e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR );

        }

    }

    /**
     *
     *
     * @return \Illuminate\Http\Response
     */
    public function cancel( Request $request ) {

        $request_data = $request->all();

        $user = auth()->user();

        if( $user->type_id !== User::COMMERCIANT ) {
            // l'utente non è un commerciant HTTP 401 Unprocessable Entity
            return $this->errorResponse( 'Unauthorized', Response::HTTP_UNAUTHORIZED );
        }

        $commerciant = Commerciant::byUserId( $user->id )->with( 'shop' )->firstOrFail();

        $validation_result = $this->cancelOfferValidator( $request_data );

        // se la validazione non ha successo
        if( !$validation_result[ 'result' ] ) {
            // restituisce gli errori di validazione con il codice HTTP 422 Unprocessable Entity
            return $this->errorResponse( $validation_result[ 'errors' ], Response::HTTP_UNPROCESSABLE_ENTITY );
        } // se l'esecuzione procede

        $offer = Offer::findOrFail( $request_data[ 'o_id' ] );

        if( $offer->o_shop_ext !== $commerciant->shop->id ) {
            // l'utente non è un commerciant HTTP 401 Unprocessable Entity
            return $this->errorResponse( 'Unauthorized', Response::HTTP_UNAUTHORIZED );
        }

        try { // tenta l'inserimento dell'ordine e delle relative relazioni

            // inizializza un transazione
            DB::beginTransaction();

            // inserisce le proprietà
            $offer->o_cancelled = Carbon::now();

            // la salva
            $offer->save();

            // committa la transazione
            DB::commit();

            // ritorna l'offerta con un codice 200 OK
            return $this->showOne( $offer,Response::HTTP_OK );

        } catch (\Exception $e) { // se genera eccezione

            // ritorna un messaggio di errore con codice 500 INTERNAL SERVER ERROR
            return $this->errorResponse( $e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR );

        }

    }

    private function insertNewOfferValidator( $data ) {
        $validationResult = [
            'result' => true,
            'errors' => []
        ];


        $offer_rules = [
            'o_title'       =>  'required|max:255',
            'o_desc'        =>  'required|max:4096',
            'o_pric_start'  =>  'numeric|between:0,999999.99',
            'o_pric'        =>  'numeric|required|between:0,999999.99',
            'o_disc'        =>  'required|numeric|between:0,100.00',
            'o_howm'        =>  'numeric|min:0',
            'o_start'       =>  'required|date_format:Y-m-d H:i:s',
            'o_end'         =>  'required|date_format:Y-m-d H:i:s',
            'o_cat'         =>  'numeric|exists:category,id',
            'o_image'       =>  'nullable|file|max:4096|mimes:jpeg,jpg,png',
        ];

        $offer_validation = Validator::make( $data, $offer_rules );

        if ( !$offer_validation->passes() ) {
            $validationResult = [
                'result' => false,
                'errors' => $offer_validation->messages()
            ];


            return $validationResult;
        }


        $offer_prices_rules = [
            'o_pric_start'  =>  'gte:o_pric',
          //  'o_pric'        =>  'lt:o_pric_start'
        ];

        $offer_validation = Validator::make( $data, $offer_prices_rules );

        if ( !$offer_validation->passes() ) {
            $validationResult = [
                'result' => false,
                'errors' => $offer_validation->messages()
            ];
        }


        return $validationResult;
    }

    private function editOfferValidator( $data ) {
        $validationResult = [
            'result' => true,
            'errors' => []
        ];

        $offer_rules = [
            'o_id'          =>  'required|numeric|exists:offer,id',
            'o_title'       =>  'required|max:255',
            'o_desc'        =>  '',
            'o_pric_start'  =>  'numeric|between:0,999999.99',
            'o_pric'        =>  'numeric|required|between:0,999999.99',
            'o_disc'        =>  'numeric|between:0,100.00',
            'o_howm'        =>  'numeric|min:0',
            'o_start'       =>  'required|date_format:Y-m-d H:i:s',
            'o_end'         =>  'required|date_format:Y-m-d H:i:s',
            'o_cat'         =>  'numeric|exists:category,id',
            'o_image'       =>  'nullable|file|max:4096|mimes:jpeg,jpg,png',
        ];

        $offer_validation = Validator::make( $data, $offer_rules );

        if ( !$offer_validation->passes() ) {
            $validationResult = [
                'result' => false,
                'errors' => $offer_validation->messages()
            ];
            return $validationResult;
        }
        $offer_prices_rules = [
            'o_pric_start'  =>  'gte:o_pric',
            //'o_pric'        =>  'lt:o_pric_start'
        ];

        $offer_validation = Validator::make( $data, $offer_prices_rules );

        if ( !$offer_validation->passes() ) {
            $validationResult = [
                'result' => false,
                'errors' => $offer_validation->messages()
            ];
        }

        return $validationResult;

    }

    private function cancelOfferValidator( $data ) {
        $validationResult = [
            'result' => true,
            'errors' => []
        ];

        $offer_rules = [
            'o_id'          =>  'required|numeric|exists:offer,id'
        ];

        $offer_validation = Validator::make( $data, $offer_rules );

        if ( !$offer_validation->passes() ) {
            $validationResult = [
                'result' => false,
                'errors' => $offer_validation->messages()
            ];
        }


        return $validationResult;

    }

    public function buyMore( Request $request ) {
        $request_data = $request->all();

        $user = auth()->user();

        if( $user->type_id !== User::COMMERCIANT ) {
            // l'utente non è un commerciant HTTP 401 Unprocessable Entity
            return $this->errorResponse( 'Unauthorized', Response::HTTP_UNAUTHORIZED );
        }

        $commerciant = Commerciant::byUserId( $user->id )->with( 'shop' )->firstOrFail();

        $validation_result = $this->buyMoreOfferValidator( $request_data );

        // se la validazione non ha successo
        if( !$validation_result[ 'result' ] ) {
            // restituisce gli errori di validazione con il codice HTTP 422 Unprocessable Entity
            return $this->errorResponse( $validation_result[ 'errors' ], Response::HTTP_UNPROCESSABLE_ENTITY );
        } // se l'esecuzione procede

        $data = [
            'commerciant'   =>  $commerciant,
            'offers'        =>  $request_data[ 'offers' ]
        ];


        //$admin_emails = User::byTypeId( User::ADMINISTRATOR )->get()->pluck( 'email' )->toArray();

        $admin_emails = $request->debug ? ["neogene@gmail.com"] : ["neogene@gmail.com","info@buybyme.net"];// explode(';', env("NOTIFICATION_EMAIL",'info@buybyme.net' ) );

        // $sent = Mail::to( $admin_emails )->send( new PurchaseOfferRequest( $data ) );
        $sent = Mail::to( $admin_emails )->send( new PurchaseOfferRequest( $data ) );

        if (count(Mail::failures()) > 0) {
            return $this->errorResponse($validation_result['errors'], Response::HTTP_INTERNAL_SERVER_ERROR);
        } else {
            return $this->successResponse("OK", Response::HTTP_OK);
        }
    }

    public function renewal( Request $request ) {
        $request_data = $request->all();

        $user = auth()->user();

        if( $user->type_id !== User::COMMERCIANT ) {
            // l'utente non è un commerciant HTTP 401 Unprocessable Entity
            return $this->errorResponse( 'Unauthorized', Response::HTTP_UNAUTHORIZED );
        }

        $commerciant = Commerciant::byUserId( $user->id )->with( 'shop' )->firstOrFail();

        //No validation required

        $data = [
            'commerciant'   =>  $commerciant,
        ];


        //$admin_emails = User::byTypeId( User::ADMINISTRATOR )->get()->pluck( 'email' )->toArray();

       // $admin_emails = explode(';',env("NOTIFICATION_EMAIL",'info@buybyme.net'));

        $admin_emails = $request->debug ? ["neogene@gmail.com"] : ["neogene@gmail.com","info@buybyme.net"];// explode(';', env("NOTIFICATION_EMAIL",'info@buybyme.net' ) );

        // $sent = Mail::to( $admin_emails )->send( new PurchaseOfferRequest( $data ) );
        $sent = Mail::to( $admin_emails )->send( new SubscriptionRenewalRequest( $data ) );

        $validation_result = null;

        if (count(Mail::failures()) > 0) {
            return $this->errorResponse($validation_result['errors'], Response::HTTP_INTERNAL_SERVER_ERROR);
        } else {
            return $this->successResponse("OK", Response::HTTP_OK);
        }
    }

    private function buyMoreOfferValidator( $data ) {
        $validationResult = [
            'result' => true,
            'errors' => []
        ];

        $offer_rules = [
            'offers'        =>  'required|numeric|min:1'
        ];

        $offer_validation = Validator::make( $data, $offer_rules );

        if ( !$offer_validation->passes() ) {
            $validationResult = [
                'result' => false,
                'errors' => $offer_validation->messages()
            ];
        }


        return $validationResult;

    }
}
