<?php

namespace App\Http\Controllers;

use App\Events\UserCreatedEvent;
use App\Models\Customer;
use App\Models\CustomerFav;
use App\Models\Offer;
use App\Models\User;

use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;


class CustomersController extends Controller {

    use ApiResponser;

    /**
     * Create a new CustomersController instance.
     *
     * @return void
     */
    public function __construct() {
        // $this->middleware('auth:api' );
    }


    public function allFavs( Request $request ) {

        try {
            $customer_id = $request->get( 'customer_id' );

            $customer =  Customer::with( [
                'favs' => function( $q ) {
                    $q->whereHas( 'shop', function( $q ) {
                        $q->whereHas( 'owner', function( $q ) {
                            $q->subscriptionActive();
                        });
                    });
                }
            ] )->findOrFail( $customer_id );

            if( $request->has( 'only_id' ) && $request->get( 'only_id') == 1 ) {
                $result = $customer->favs->pluck( 'id' )->toArray();
            } else {
                $result = $customer->favs;
            }

            return response()->json( $result );
        }
        catch (\Exception $e){
            return response()->json( [] );
        }
    }

    public function setFav( Request $request ) {

        try {
            $request_data = $request->all();

            $customer_id = $request_data['customer_id'];
            $offer_id = $request_data['offer_id'];
            $state = $request_data['state'];


            $customer = Customer::findOrFail($customer_id);
            $offer = Offer::findOrFail($offer_id);

            $fav_exists = CustomerFav::byOffer($offer_id)->byCustomer($customer->id)->first();

            if ($state == 1 && !$fav_exists) {
                $attach_result = $customer->favs()->sync([$offer_id], false);
                $result = count($attach_result['attached']) > 0 ? 1 : 0;
            } elseif ($state == 0 && $fav_exists) {
                $result = $customer->favs()->detach([$offer_id]);
            } else {
                $result = -1;
            }

            return response()->json($result);
        }
         catch (\Exception $e){
            return response()->json( 0 );
        }
    }

    public function setLike( Request $request ) {

        try {
            $request_data = $request->all();

            $customer_id = $request_data['customer_id'];
            $offer_id = $request_data['offer_id'];
            $state = $request_data['state'];

            $customer = Customer::findOrFail($customer_id);
            $offer = Offer::findOrFail($offer_id);

            $like_exists = $customer->likedOffers->contains($offer_id);

            if ($state == 1 && !$like_exists) {
                $attach_result = $customer->likedOffers()->sync([$offer_id], false);
                $result = count($attach_result['attached']) > 0 ? 1 : 0;
                $offer->increment( 'o_likes', 1 );
            } elseif ($state == 0 && $like_exists) {
                $result = $customer->likedOffers()->detach([$offer_id]);
                $offer->decrement( 'o_likes', 1 );
            } else {
                $result = -1;
            }

            return response()->json($result);
        }
        catch (\Exception $e){
            return response()->json( 0 );
        }
    }

    public function allLikes( Request $request ) {

        try {
            $customer_id = $request->get( 'customer_id' );

            $customer =  Customer::with( [
                'likedOffers' => function( $q ) {
                    $q->whereHas( 'shop', function( $q ) {
                        $q->whereHas( 'owner', function( $q ) {
                            $q->subscriptionActive();
                        });
                    });
                }
            ] )->findOrFail( $customer_id );

            if( $request->has( 'only_id' ) && $request->get( 'only_id') == 1 ) {
                $result = $customer->likedOffers->pluck( 'id' )->toArray();
            } else {
                $result = $customer->likedOffers;
            }

            return response()->json( $result );
        }
        catch (\Exception $e){
            return response()->json( [] );
        }
    }

    /**
     * @param Request $request
     */
    public function setPrefs(Request $request){
        $request_data = $request->all();

        $validation_result = $this->createPrefsFullValidator( $request_data );

        // se la validazione non ha successo
        if( !$validation_result[ 'result' ] ) {
            // restituisce gli errori di validazione con il codice HTTP 422 Unprocessable Entity
            return $this->errorResponse( $validation_result[ 'errors' ], Response::HTTP_UNPROCESSABLE_ENTITY );
        } // se l'esecuzione procede

        try { // tenta l'inserimento degli aggiornamenti delle preferenze

            $userID = $request_data[ 'user_id' ];

            $user = User::findOrFail( $userID );

            if( $user->type_id !== User::CUSTOMER ) {
                return $this->successResponse("KO",Response::HTTP_UNAUTHORIZED);
            }

            $customer = Customer::byUserId($userID)->firstOrFail();

            DB::beginTransaction();

                if (isset( $request_data[ 'u_notification_discounts' ] )) {
                    $customer->u_notification_discounts = $request_data['u_notification_discounts'];
                }

                if (isset( $request_data[ 'u_notification_categories' ] )) {
                   $customer->u_notification_categories = $request_data['u_notification_categories'];
               }

                //if (isset( $request_data[ 'u_notification_provincia' ] )) {
                    $customer->u_notification_provincia = $request_data['u_notification_provincia'];
                //}

               if (isset( $request_data[ 'u_notification_only_filters' ] )) {
                   $customer->u_notification_only_filters = $request_data['u_notification_only_filters'];
               }

                if (isset( $request_data[ 'u_notification_ios_on' ] )) {
                    $customer->u_notification_ios_on = $request_data['u_notification_ios_on'];
                }
                if (isset( $request_data[ 'u_notification_android_on' ] )) {
                    $customer->u_notification_android_on = $request_data['u_notification_android_on'];
                }
                if (isset( $request_data[ 'u_notification_ios_token' ] )) {
                    $customer->u_notification_ios_token = $request_data['u_notification_ios_token'];
                }
                if (isset( $request_data[ 'u_notification_android_token' ] )) {
                    $customer->u_notification_android_token = $request_data['u_notification_android_token'];
                }

                $customer->save();

            // committa la transazione
            DB::commit();

            // ritorna codice 201 CREATED
            return $this->successResponse("OK",Response::HTTP_CREATED);
            //return $this->showAggregatedResult( collect( [ $user, $commerciant_details, $shop ] ),Response::HTTP_CREATED );

        } catch (\Exception $e) { // se genera eccezione

            if (strpos(strtolower($e->getMessage()),"email")!==false){
                return $this->errorResponse( "email", Response::HTTP_INTERNAL_SERVER_ERROR );
            }
            // ritorna un messaggio di errore con codice 500 INTERNAL SERVER ERROR
            return $this->errorResponse( $e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR );

        }
    }

    /**
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function createFull( Request $request ) {

        $request_data = $request->all();

        $validation_result = $this->createUserFullValidator( $request_data );

        // se la validazione non ha successo
        if( !$validation_result[ 'result' ] ) {
            // restituisce gli errori di validazione con il codice HTTP 422 Unprocessable Entity
            return $this->errorResponse( $validation_result[ 'errors' ], Response::HTTP_UNPROCESSABLE_ENTITY );
        } // se l'esecuzione procede


        try { // tenta l'inserimento dell'ordine e delle relative relazioni

            // inizializza un transazione
            DB::beginTransaction();

            $user = new User;

            $user->fullname = $request_data[ 'u_name' ] . ' ' . $request_data[ 'u_last_name' ];
            $user->password = bcrypt( $request_data[ 'password' ] );
            $user->email = $request_data[ 'email' ];
            $user->type_id = User::CUSTOMER;
            $user->activation_token = str_random( 64 );
            $user->save();

            $customer_details = new Customer();

            $customer_details->u_name = $request_data[ 'u_name' ];
            $customer_details->u_last_name = $request_data[ 'u_last_name' ];
            $customer_details->user_id = $user->id;
            $customer_details->u_prov = $request_data[ 'u_prov' ];
            $customer_details->u_prov_code = $request_data[ 'u_prov_code' ];
            $customer_details->u_notification_ios_token = isset( $request_data[ 'u_notification_ios_token' ] ) ? $request_data[ 'u_notification_ios_token' ] : null;
            $customer_details->u_notification_android_token = isset( $request_data[ 'u_notification_android_token' ] ) ? $request_data[ 'u_notification_android_token' ] : null;
            $customer_details->u_notification_ios_on = 1;
            $customer_details->u_notification_android_on = 1;
            $customer_details->save();

            // committa la transazione
            DB::commit();



            event( new UserCreatedEvent( $user ) );
            // ritorna codice 201 CREATED
            return $this->successResponse("OK",Response::HTTP_CREATED);
            //return $this->showAggregatedResult( collect( [ $user, $commerciant_details, $shop ] ),Response::HTTP_CREATED );

        } catch (\Exception $e) { // se genera eccezione

            if (strpos(strtolower($e->getMessage()),"email")!==false){
                return $this->errorResponse( "email", Response::HTTP_INTERNAL_SERVER_ERROR );
            }
            // ritorna un messaggio di errore con codice 500 INTERNAL SERVER ERROR
            return $this->errorResponse( $e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR );

        }
    }

    private function createUserFullValidator( $data ) {

        $validationResult = [
            'result' => true,
            'errors' => []
        ];

        $user_rules = [
            'email'                        =>   'required|unique:users|max:191',
            'password'                     =>   'required|min:8|max:192',
            'u_name'                       =>   'required|min:2|max:50',
            'u_last_name'                  =>   'nullable|max:50',
            'u_prov'                       =>   'required|min:2|max:50',
            'u_prov_code'                  =>   'required|alpha|min:2|max:2',
            'u_notification_ios_token'     =>   'nullable|min:20|max:255',
            'u_notification_android_token' =>   'nullable|min:20|max:255',
        ];

        $messages = array(
            'email.required' => 'Email mancante',
            'email.unique' => 'Email già utilizzata',
            'password.required' => 'Password mancante',
            'password.min' => 'La password deve essere lunga almeno 8 caratteri',
            'password.max' => 'La password non può essere più lunga di 192 caratteri',
            'u_name.required' => 'Il nome non può essere vuoto',
            'u_name.min' => 'Il nome deve avere una lunghezza di 2 o più caratteri',
            'u_name.max' => 'Il nome non può avere una lunghezza superiore ai 50 caratteri',
            'u_last_name.max' => 'Il cognome non può avere una lunghezza superiore ai 50 caratteri',
            'u_prov.required' => "Provincia mancante",
            'u_prov_code.required' => "Codice provincia mancante"
        );

        $user_validation = Validator::make( $data, $user_rules,$messages);

        if ( !$user_validation->passes() ) {
            $validationResult = [
                'result' => false,
                'errors' => $user_validation->messages()
            ];
        }


        return $validationResult;
    }

    private function createPrefsFullValidator($data){
        $validationResult = [
            'result' => true,
            'errors' => []
        ];


        /*
         *  "": Manager.userInfo["id"]!,
        "":sconti,
        "":categorie,
        "":Manager.userInfo["provsigla"] ?? "",
        "":Manager.notificationsOnlyFiltered ? "1" : "0"
         *
         */

        $user_rules = [
            'user_id'                      => 'required|numeric',
            'u_notification_discounts'     => 'nullable|min:18|max:18',
            'u_notification_categories'    => 'nullable|min:30|max:30',
            'u_notification_provincia'     => 'nullable|max:2',
            'u_notification_only_filters'  => 'nullable|boolean|min:1|max:1',
            'u_notification_ios_on'        => 'nullable|boolean|min:1|max:1',
            'u_notification_android_on'    => 'nullable|boolean|min:1|max:1',
            "u_notification_ios_token"     => 'nullable|min:20|max:255',
            "u_notification_android_token" => 'nullable|min:20|max:255',
        ];

        $user_validation = Validator::make( $data, $user_rules  );

        if ( !$user_validation->passes() ) {
            $validationResult = [
                'result' => false,
                'errors' => $user_validation->messages()
            ];
        }


        return $validationResult;
    }
}
