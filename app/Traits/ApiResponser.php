<?php

namespace App\Traits;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

trait ApiResponser {

    private function successResponse( $data, $code ) {
        return response()->json( [ 'result' => $data, 'code' => $code ], $code );
    }

    protected function errorResponse( $data, $code ) {
        return response()->json( [ 'error' => $data, 'code' => $code ], $code );
    }

    protected function showAll( Collection $collection, $code = 200 ) {
        return $this->successResponse( [ 'data' => $collection ], $code );
    }

    protected function showOne( Model $model, $code = 200 ) {
        return $this->successResponse( [ 'data' => $model ], $code );
    }

    protected function showAggregatedResult( Collection $collection, $code = 200 ) {
        return $this->successResponse( [ 'data' => $collection ], $code );
    }
}