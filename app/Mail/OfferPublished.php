<?php

namespace App\Mail;

use App\Models\Commerciant;
use App\Models\Offer;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class OfferPublished extends Mailable {
    use Queueable, SerializesModels;

    public $offer;
    public $commerciant;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct( Offer $offer, Commerciant $commerciant ) {
        $this->offer = $offer;
        $this->commerciant = $commerciant;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build() {
        return $this->view('mail.offer-published');
    }
}
