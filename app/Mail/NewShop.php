<?php

namespace App\Mail;

use App\Models\Commerciant;
use App\Models\Shop;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NewShop extends Mailable {

    use Queueable, SerializesModels;

    public $shop;
    public $commerciant;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct( Shop $shop, Commerciant $commerciant ) {
        $this->shop = $shop;
        $this->commerciant = $commerciant;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build() {
        return $this->view('mail.new-shop');
    }
}
