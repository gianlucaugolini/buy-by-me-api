<?php

namespace App\Mail;

use App\Models\Category;
use App\Models\Catena;
use App\Models\CentroCommerciale;
use App\Models\Commerciant;
use App\Models\Shop;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class UpdateShopAndCommerciantDetails extends Mailable {

    use Queueable, SerializesModels;

    public $shop;
    public $commerciant;
    public $user;
    public $old_data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct( $old_data, Shop $shop, Commerciant $commerciant, User $user ) {
        $this->shop         = $shop;
        $this->commerciant  = $commerciant;
        $this->user         = $user;
        $this->old_data     = $old_data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build() {

        $old_cat = $this->old_data[ 'old_shop' ]->categoria;
        $new_cat = Category::find( $this->shop->s_cat );

        $old_catena = $this->old_data[ 'old_shop' ]->catena;
        $new_catena = !is_null( $this->shop->s_catena_commerciale_ext ) ? Catena::find( $this->shop->s_catena_commerciale_ext ) : null;

        $old_cc = $this->old_data[ 'old_shop' ]->centroCommerciale;
        $new_cc = !is_null( $this->shop->s_centro_commerciale_ext ) ? CentroCommerciale::find( $this->shop->s_centro_commerciale_ext ) : null;

        return $this
            ->view('mail.edit-shop')
            ->with([
                'shop'          => $this->shop,
                'commerciant'   => $this->commerciant,
                'user'          => $this->user,
                'old_data'      => $this->old_data,
                'new_cat'       => $new_cat,
                'old_cat'       => $old_cat,
                'old_catena'    =>  $old_catena,
                'new_catena'    =>  $new_catena,
                'old_cc'        =>  $old_cc,
                'new_cc'        =>  $new_cc,
            ]);
    }
}
