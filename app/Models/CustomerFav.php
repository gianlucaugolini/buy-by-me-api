<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CustomerFav extends Model {

    protected $table = 'user_favs';
    protected $primaryKey = 'id'; // or null
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'uf_offerid_ext',
        'uf_userid_ext',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ["updated_at"];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'created_at' => 'timestamp',
        'updated_at' => 'timestamp',
    ];

    /**
     * L'offerta
     */
    public function offer() {
        return $this->belongsTo('App\Models\Offer', 'uf_offerid_ext', 'id' );
    }

    /**
     * L'offerta
     */
    public function user() {
        return $this->belongsTo('App\Models\User', 'uf_userid_ext', 'id' );
    }


    public function scopeByCustomer( $query, $id ) {
        return $query->where( [ 'uf_userid_ext' => $id ] );
    }

    public function scopeByOffer( $query, $id ) {
        return $query->where( [ 'uf_offerid_ext' => $id ] );
    }

}
