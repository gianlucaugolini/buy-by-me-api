<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable  implements JWTSubject {

    use Notifiable;


    const ADMINISTRATOR = 1;
    const CUSTOMER = 2;
    const COMMERCIANT = 3;
    const SUPERVISOR = 4;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'fullname', 'email', 'password', 'type_id', 'activation_token', 'api_password_reset_token', 'logged'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
       'api_id','password', 'remember_token','u_blocked','u_confirmed','u_token','updated_at','email_verified_at','is_superadmin','api_password_reset_token','logged'
    ];


    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'created_at'        => 'timestamp',
        'updated_at'        => 'timestamp',
        'email_verified_at' => 'timestamp',
    ];

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier() {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims() {

        switch ( $this->type_id ) {
            case self::CUSTOMER :
                $detail_table_id = Customer::byUserId( $this->id )->first()->id;
                break;
            case self::COMMERCIANT :
                $detail_table_id = Commerciant::byUserId( $this->id )->first()->id;
                break;
            case self::SUPERVISOR :
                $detail_table_id = Supervisor::byUserId( $this->id )->first()->id;
                break;
            default:
                $detail_table_id = 0;
        }


        return [
            'userdata' => [
                'id'                =>  $this->id,
                'name'              =>  $this->fullname,
                'email'             =>  $this->email,
                'type'              =>  $this->type_id,
                'detail_table_id'   =>  $detail_table_id
            ]
        ];
    }

    public function scopeByTypeId( $query, $id ) {
        return $query->where( 'type_id', $id );
    }

    public function scopeByLogged( $query, $logged = 1) {
        return $query->where( 'logged', $logged);
    }

    public function scopeByActivationToken( $query, $activation_token ) {
        return $query->where( 'activation_token', $activation_token );
    }

    public function scopeByApiPasswordResetToken( $query, $password_reset_token ) {
        return $query->where( 'api_password_reset_token', $password_reset_token );
    }
}
