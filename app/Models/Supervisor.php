<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Supervisor extends Model {

    protected $table = 'supervisor';
    protected $primaryKey = 'id'; // or null
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'ccs_name',
        'ccs_last_name',
        'cc_ext',
        'ccs_blocked',
    ];


    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'created_at' => 'timestamp',
        'updated_at' => 'timestamp',
    ];

    public function user() {
        return $this->belongsTo('App\Models\User', 'user_id', 'id' );
    }

    /**
     * La subscription a cui il Contatto è legato
     */
    public function catena() {
        return $this->belongsTo('App\Models\Catena', 'cc_ext', 'id' );
    }

    public function scopeNotBlocked( $query ) {
        return $query->where( 'ccs_blocked', '<>', 1 );
    }

    public function scopeByUserId( $query, $id ) {
        return $query->where( 'user_id', $id );
    }

    public function scopeNoSupervision( $query ) {
        return $query
            ->where( 'cc_ext', null )
            ->orWhere( 'cc_ext', 0 );
    }

    public function getBlockedLabelAttribute() {
        return $this->ccs_blocked === 0 ? 'NO' : 'SI';
    }
}