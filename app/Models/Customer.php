<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model {

    protected $table = 'customer';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'u_name',
        'user_id',
        'u_last_name',
        'u_prov',
        'u_prov_code',
        'u_blocked',
        'u_confirmed',
        'u_token',
        'u_notification_ios_token',
        'u_notification_android_token',
        'u_notification_ios_on',
        'u_notification_android_on',
        'u_notification_provincia',
        'u_notification_only_filters',
        'u_notification_discounts',
        'u_notification_categories',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'created_at' => 'timestamp',
        'updated_at' => 'timestamp',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ["created_at","updated_at",'u_confirmed','u_blocked','u_token','updated_at',"u_notification_ios_token","u_notification_android_token"];

    public function user() {
        return $this->belongsTo('App\Models\User' );
    }

    public function favs() {
        return $this->belongsToMany('App\Models\Offer','user_favs', 'uf_userid_ext', 'uf_offerid_ext')->withTimestamps();
    }

    public function likedOffers() {
        return $this->belongsToMany('App\Models\Offer', 'users_likes','ul_userid_ext','ul_offerid_ext')->withTimestamps();
    }

    public function scopeByUserId( $query, $id ) {
        return $query->where( 'user_id', $id );
    }

    public function scopeNotBlocked( $query ) {
        return $query->where( 'u_blocked', '<>', 1 );
    }

    public function getBlockedLabelAttribute() {
        return $this->u_blocked === 0 ? 'NO' : 'SI';
    }

    public function getConfirmedLabelAttribute() {
        return $this->u_confirmed === 0 ? 'NO' : 'SI';
    }
}
