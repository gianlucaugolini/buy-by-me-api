<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CentroCommerciale extends Model {

    protected $table = 'centro_commerciale';
    protected $primaryKey = 'id'; // or null
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'cc_name'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ["created_at","updated_at"];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'created_at' => 'timestamp',
        'updated_at' => 'timestamp',
    ];


    /**
     * Gli shop del Centro Commerciale
     */
    public function shops() {
        return $this->hasMany('App\Models\Shop', 's_centro_commerciale_ext', 'id' );
    }
}
