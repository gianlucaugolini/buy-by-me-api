<?php

namespace App\Helpers;
use App\Models\Offer;
use Illuminate\Support\Facades\DB;
use Edujugon\PushNotification\PushNotification;
use App\Models\Customer;

abstract class PushNotificationsTargetFamily {
    const All = 0;
    const Apple = 1;
    const Android = 2;
    // etc.
}

class PushNotificationsHelper {

    /**
     * @param null $id
     * @param bool $debug
     * @return string
     */
    public static function sendPushNotificationForOfferWithID($id = null, $debug = false){

        if ($id != null){
            //query
            $offer =  Offer::findOrFail( $id );

            return PushNotificationsHelper::sendPushNotificationForOffer($offer, $debug);
        }

        return "";
    }

    public static function sendPushNotificationForOffer($offer = null, $debug = false) {

        if ($offer == null){
            return "";
        }

        $DEV = false;

        $message = $offer->shop->s_name ." ha pubblicato: ". $offer->o_title;
        $cat = $offer->o_cat - 1;    //1-15  so $cat-1 is array index of splitted 1/0;1/0; etc
        $provincia = $offer->shop->s_prov_code;
        $discount = $offer->o_disc; //value is 0% - 100%, recorded is 1;1;1;1;1;1;1;1;1 == less than or eq 10%,20%....90%

        if ($discount%10!=0){
            $discount = $discount - $discount%10; //makes discount multiple of 10 eg: 15 - 15%5 (=5) = 10
        }

        $discount = $discount/10 - 1; //20% /10 = 2nd index - 1 = index 1 (array is 0...9, 0 = 10% ...)

        //1 query to get list of all tokens of users which respect $cats

        if ($message !=null && $cat >= 0 && $cat<=14 && $provincia != null) {

            $result = "";

            $tokensiOS = $debug ?
                Customer::whereHas(
                    'user', function ($query) {
                    $query->where('email', 'neogene@tin.it');
                })

                    ->whereNotNull('u_notification_ios_token')
                    ->select("u_notification_ios_token")
                    ->pluck("u_notification_ios_token")
                    ->toArray()
                :
                Customer::whereHas(
                    'user', function ($query) {
                    $query->where('logged', '=', '1')->where('email', '<>', 'p@p.it'); //debug disable request;
                })
                ->where('u_notification_ios_on', '1')
                ->whereNotNull('u_notification_ios_token')
                ->where(function($q) use ($cat,$discount,$provincia) {
                    $q->where('u_notification_only_filters', '0')
                    ->orwhere(function($q)  use ($cat,$discount,$provincia) {
                        $q->where('u_notification_only_filters', '1')
                            ->whereRaw("SUBSTRING_INDEX(SUBSTRING_INDEX(u_notification_categories, ';', $cat), ';', 0)='1'")
                            ->whereRaw("SUBSTRING_INDEX(SUBSTRING_INDEX(u_notification_discounts, ';', $discount), ';', 0)='1'")
                            ->where("u_notification_provincia","$provincia");

                    });
                })
                ->select("u_notification_ios_token")
                ->pluck("u_notification_ios_token")
                ->toArray();

            $tokensAndroid =
                $debug ?
                    Customer::whereHas(
                        'user', function ($query) {
                        $query->where('email', 'neogene@tin.it');
                    })
                        ->whereNotNull('u_notification_android_token')
                        ->select("u_notification_android_token")
                        ->pluck("u_notification_android_token")
                        ->toArray()
                :
                    Customer::whereHas(
                        'user', function ($query) {
                        $query->where('logged', '=', '1')->where('email', '<>', 'p@p.it');;
                    })
                    ->where('u_notification_android_on', '1')
                    ->whereNotNull('u_notification_android_token')
                    ->where(function($q) use ($cat,$discount,$provincia) {
                        $q->where('u_notification_only_filters', '0')
                            ->orwhere(function($q)  use ($cat,$discount,$provincia) {
                                $q->where('u_notification_only_filters', '1')
                                    ->whereRaw("SUBSTRING_INDEX(SUBSTRING_INDEX(u_notification_categories, ';', $cat), ';', 0)='1'")
                                    ->whereRaw("SUBSTRING_INDEX(SUBSTRING_INDEX(u_notification_discounts, ';', $discount), ';', 0)='1'")
                                    ->where("u_notification_provincia","$provincia");

                            });
                    })
                ->select("u_notification_android_token")
                ->pluck("u_notification_android_token")
                ->toArray();

            $tokens = array_filter(array_merge($tokensiOS,$tokensAndroid));

            $push = new PushNotification('fcm');

            $push
            ->setMessage([
                    'notification' => [
                        'title'=>'BuyByMe'.($debug ? "::DEBUG":""),
                        'body'=>$message,
                        'sound' => 'default',
                        'o_uid' => $offer->o_uid,
                        'click_action' => 'NEW_OFFER_SHOW_SHOP'
                    ],
                    'data' => [
                        'o_uid' => $offer->o_uid,
                        'click_action' => 'NEW_OFFER_SHOW_SHOP'
                    ]
                ]
           )
            ->setConfig(['dry_run' => $DEV,'priority' => 'high'])
            ->setApiKey('AAAAXTZTr1M:APA91bGQMu3ecx_QmvWPabrIjKymwIojW7XK7tJqi6t27jd-6p-3l6cERk_LDk0w43wF2EX27BWfuKDdv3YVERlzLyG8gpcfV8dXMtcnV0ZoZdJj50BNaUDHCUUAuBziTjSvP6efXIXW')
            ->setDevicesToken($tokens)
            ->sendByTopic('buybyme')
            ->send();

            //$result.= var_dump($tokens);
            //$result .= "Sending:$message of offer:".$offer->id." <br>";
            //$result.=var_dump(get_object_vars($push->getFeedback()));

            /*
            if (array_key_exists("failure",$push->getFeedback())){
                $failure = $push->getFeedback()->failure;

                if ($failure == 1){
                    $result.=var_dump(json_encode($push->getFeedback()));
                }
            }
            */
            //$result.=json_encode(implode("######",$tokens));

            //iOS tokens
           /* $queryiOS = "SELECT u_notification_ios_token as token FROM customer WHERE ";
            $queryiOS.= " u_notification_ios_token <> null AND u_notification_ios_on = 1 ) AND (u_notification_only_filters = 0 ";
            $queryiOS.= " OR (u_notification_only_filters = 1";
            $queryiOS.= " AND u_notification_ios_on = 1 ";
            $queryiOS.= " AND SUBSTRING_INDEX(SUBSTRING_INDEX(u_notification, ';', $cat), ';', 0)='1'";
            $queryiOS.= " AND SUBSTRING_INDEX(SUBSTRING_INDEX(u_notification_discounts, ';', $discount), ';', 0)='1'";
            $queryiOS.= " AND u_notification_provincia = '$provincia'))";

            //Android tokens
            $queryAndroid = "SELECT u_notification_android_token as token FROM customer WHERE ";
            $queryAndroid.= " (u_notification_android_token <> null ";
            $queryAndroid.= " AND u_notification_only_filters = 0 ";
            $queryAndroid.= " AND u_notification_android_on = 1 ) ";
            $queryAndroid.= " OR (u_notification_android_token <> null ";
            $queryAndroid.= " AND u_notification_only_filters = 1 ";
            $queryAndroid.= " AND u_notification_android_on = 1 ";
            $queryAndroid.= " AND SUBSTRING_INDEX(SUBSTRING_INDEX(u_notification, ';', $cat), ';', 0)='1'";
            $queryAndroid.= " AND SUBSTRING_INDEX(SUBSTRING_INDEX(u_notification_discounts, ';', $discount), ';', 0)='1'";
            $queryAndroid.= " AND u_notification_provincia = '$provincia')";
            */

            //2 iterate and send notification to each token

            return "Sent to: ".count($tokens)." devices.";
        }
        return "";

    }

}
