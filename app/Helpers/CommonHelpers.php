<?php

namespace App\Helpers;

class CommonHelpers {

    /**
     * @param $array
     */
    public static function fprint( $array ) {
        print '<pre>';
        print_r( $array );
        print '</pre>';
    }

    /**
     * @param $array
     */
    public static function fdump( $array ) {
        print '<pre>';
        var_dump( $array );
        print '</pre>';
    }


    /**
     * @param $array
     * @param $key
     * @param bool $string
     * @param bool $asc
     */
    public static function array_sort_by_key( &$array, $key, $string = false, $asc = true ){
        /**
            EXAMPLE
         *  array_sort_by_key($yourArray,"name",true); //String sort (ascending order)
         *  array_sort_by_key($yourArray,"name",true,false); //String sort (descending order)
         *  array_sort_by_key($yourArray,"id"); //number sort (ascending order)
         *  array_sort_by_key($yourArray,"count",false,false); //number sort (descending order)
         *
         */
        if( $string ){
            usort($array,function ($a, $b) use( &$key, &$asc ) {
                if ($asc) {
                    return strcmp(strtolower($a{$key}), strtolower($b{$key}));
                } else {
                    return strcmp(strtolower($b{$key}), strtolower($a{$key}));
                }
            });
        } else {
            usort($array, function ( $a, $b ) use ( &$key, &$asc )  {
                if ($a[$key] == $b{$key}){
                    return 0;
                }
                if ($asc) {
                    return ($a{$key} < $b{$key}) ? -1 : 1;
                } else {
                    return ($a{$key} > $b{$key}) ? -1 : 1;
                }

            });
        }
    }
}