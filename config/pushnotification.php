<?php

return [
  'gcm' => [
      'priority' => 'normal',
      'dry_run' => true,
      'apiKey' => 'My_ApiKey',
  ],
  'fcm' => [
        'priority' => 'normal',
        'dry_run' => false,
        'apiKey' => 'AAAAXTZTr1M:APA91bGQMu3ecx_QmvWPabrIjKymwIojW7XK7tJqi6t27jd-6p-3l6cERk_LDk0w43wF2EX27BWfuKDdv3YVERlzLyG8gpcfV8dXMtcnV0ZoZdJj50BNaUDHCUUAuBziTjSvP6efXIXW',
  ],
  'apn' => [
      'certificate' => __DIR__ . '/iosCertificates/PushChatCertDEV.pem', //CHANGE DRY_RUN TO FALSE FOR STORE
      //'passPhrase' => '1234', //Optional
      //'passFile' => __DIR__ . '/iosCertificates/yourKey.pem', //Optional
      'dry_run' => true //FALSE IF STORE!!!!
  ]
];
